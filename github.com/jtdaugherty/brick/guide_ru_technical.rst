Руководство пользователя Brick
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. contents:: `Table of Contents`

Введение
========

``brick`` - библиотека Haskell для программирования пользовательских интерфейсов терминала.
Её основная цель - сделать разработку терминального пользовательского интерфейса безболезненной и максимально прямой. ``brick`` построен на `vty`_; `vty` предоставляет интерфейс ввода и вывода терминала и примитивы рисования, в то время как(while) ``brick`` построен на тех, которые обеспечивают высоко-уровневую абстракцию и комбинаторы для выражения макетов(layouts) пользовательского интерфейса.

Эта документация предназначена для обеспечения высоко-уровнего обзора дизайна библиотеки наряду с руководством по ее использованию, но подробные сведения о конкретных функциях можно найти в документации Haddock.

Процесс написания приложения с использованием ``brick`` влечет(entails) за собой создание двух важных функций:

- *drawing function*, которая превращает(turns) ваше состояние приложения в спецификацию того, как должен выглядеть ваш интерфейс, и

- *event handler*, которая принимает состояние вашего приложения и входное событие и решает, нужно(whether) ли изменять состояние или выйти из программы.

Мы пишем функции рисования в ``brick``, используя обширный набор примитивов и комбинаторов для размещения текста на экране, установки его атрибутов (например, цвета переднего плана) и выражений макета (например, заполнение, центрирование, макеты полей, прокрутка видовых экранов, и т.д.).

Эти функции попадают в структуру, которую мы передаем в основной цикл событий библиотеки ``brick``. Мы подробно рассмотрим это в разделе  `Тип App`_.

Установка
---------

``brick`` может быть установлен в "обычный путь(способом(usual way)),"либо путем установки последней версии(release) `Hackage`_, либо путем клонирования репозитория GitHub и создания(building) локально.

Чтобы установить из Hackage::

   $ cabal update
   $ cabal install brick

Чтобы клонировать и собрать(build) локально::

   $ git clone https://github.com/jtdaugherty/brick.git
   $ cd brick
   $ cabal new-build


Сборка(building) демонстрационных программ
------------------------------------------

``brick`` включает большую коллекцию возможность-определённых(feature-specific) демонстрационных программ. Эти программы не собраны(built) по умолчанию, но могут быть собраны путем передачи(passing) флага ``demos`` в ``cabal install``, например::

   $ cabal install brick -f demos

Соглашения(Conventions)
=======================

``brick`` имеет некоторые соглашения(conventions) API, о которых стоит знать(worth knowing about), когда(as) вы читаете эту документацию, и когда вы исследуете(explore) исходник(source) библиотеки и пишете свои собственные программы.

- Использование пакетов `microlens`_: ``brick`` использует семейство пакетов ``microlens`` внутри(internally), а также предоставляет(exposes) линзы(lenses) для многих типов в библиотеке. Однако, если вы предпочитаете не использовать интерфейс линзы(lens) в своей программе, все интерфейсы линз(lens) имеют не-линзовые(non-lens) эквиваленты, экспортируемые одним и тем же(same) модулем. В общем(general), суффикс "``L``" на чем-либо(something) говорит вам, что это есмь линза(lens); имя без суффикса "``L``" есмь не-линзовая(non-lens) версия. Вы можете обойтись(get by) без использования ``brick``-сового интерфейса линзы(lens), но ваша жизнь, вероятно, будет намного приятнее, когда(once) ваше состояние приложения станет достаточно(sufficiently) сложным(complex), если вы используете линзы для его изменения (см. `appHandleEvent: Обработка событий(Handling Events)`_).
- Имена атрибутов: некоторые модули экспорта имен атрибутов(export attribute names) (см. `How Attributes Work`_) связанных(associated) с элементами пользовательского интерфейса. Они, как правило(tend), заканчиваются суффиксом "``Attr``" (e.g. ``borderAttr``). Кроме того, иерархические отношения между атрибутами задокументированы в Haddock документации.
- Использование квалифицированных Haskell идентификаторов: В этом документе, где разумно, я буду использовать полностью квалифицированные(fully-qualified) идентификаторы всякий раз, когда(whenever) я упоминаю что-то в первый раз или всякий раз, когда я использую то, что не является частью ``brick``. Использование квалифицированных(полных, уточненных, qualified) имен предназначено(intended) не для создания(produce) исполняемых(executable) примеров(examples), а(but) скорее, чтобы направлять(помочь, guide) вас в написании ваших операторов(statements) ``import``.

Компиляция Brick приложений
===========================

Brick-приложения должны быть скомпилированы с threaded RTS используя GHC опцию ``-threaded``.

Тип App
=======

Чтобы использовать библиотеку, мы должны предоставить ей значение типа ``Brick.Main.App``. Этот тип есмь тип записи, поля которого выполняют различные функции:

.. code:: haskell

   data App s e n =
       App { appDraw         :: s -> [Widget n]
           , appChooseCursor :: s -> [CursorLocation n] -> Maybe (CursorLocation n)
           , appHandleEvent  :: s -> BrickEvent n e -> EventM n (Next s)
           , appStartEvent   :: s -> EventM n s
           , appAttrMap      :: s -> AttrMap
           }

Тип ``App`` параметризуется(is parameterized) по(over) трём типам. Эти переменные типа будут отображаться(появятся(appear)) в сигнатурах многих библиотечных функций и типов. Они есмь(They are):

- **тип состояния приложения(application state type)** ``s``: тип данных, который будет развиваться(evolve) в(over) ходе выполнения приложения. Ваше приложение предоставит(provide) библиотеке начальное значение, а обработка событий преобразует(transform) ее по мере выполнения программы. Когда приложение `` brick`` завершается, возвращается окончательное состояние приложения.
- **тип события** ``e``: тип пользовательских событий приложений(custom application events), которые ваше приложение должно будет создавать(need to produce) и обрабатывать в ``appHandleEvent``.  Все приложения будут снабжены(provided) событиями из библиотеки ``vty``, такие как события клавиатуры или изменения размера; эта переменная типа указывает тип *дополнительных(additional)* событий, которые потребуются приложениям. Для более подробных деталей, смотрите `Использование собственного(Your Own) типа события`_.
- **Имя ресурса(resource name type)** ``n``: во время выполнения приложения нам иногда нужен способ ссылаться на состояние воспроизведения(rendering), например, пространство, занимаемое данным виджетам, состояние для прокручиваемого видового экрана, щелчка мыши или положение курсора.

Различные(various) поля App будут описаны(described) в следующих разделах(sections).

Запуск(Running) приложения
--------------------------

Чтобы выполнить ``App``, мы передаём его в  ``Brick.Main.defaultMain`` или ``Brick.Main.customMain`` наряду(along) с начальным значением состояния приложения:

.. code:: haskell

   main :: IO ()
   main = do
     let app = App { ... }
         initialState = ...
     finalState <- defaultMain app initialState
     -- Use finalState and exit

Функция customMain предназначена(is) для более расширенного(advanced) использования; подробности(for details) см. `Использование собственного(Your Own) типа события`_.


appDraw: Рисование интерфейса(Drawing an Interface)
---------------------------------------------------

Значение ``appDraw`` есмь функция, которая превращает(turns) текущее состояние приложения в список *слоев(layers)* типа ``Widget`` сначала перечислены самые верхние(listed topmost first), которые будут составлять(make up) интерфейс. Каждый ``Widget`` превращается(gets turned) в ``vty`` слой, и полученные слои притягиваются к терминалу.

Тип ``Widget`` есмь тип *инструкций рисования(drawing instructions)*. Тело вашей функции рисования будет использовать одну или несколько функций рисования чтобы построить(to build) или преобразовать значения ``Widget`` для описания(to describe) вашего интерфейса. Эти инструкции будут выполнены(will then be executed) в отношении трех вещей:

- Размер терминала: размер терминала определяет, как много значений ``Widget`` поступит(действуют (behave)). Например, значения ``Widget`` фиксированного размера, такие как текстовые строки, ведут себя(behave) одинаково при любых условиях (и обрезаются, если терминал слишком мал), но слой(layouts) комбинаторов, такие как ``Brick.Widgets.Core.vBox`` или ``Brick.Widgets.Center.center`` использует размер терминала, чтобы определить, как выложить другие виджеты. См. `Как работают виджеты и представление(Rendering)`_.
- Карта атрибутов приложения (``appAttrMap``): функции рисования, запрашивающие использование атрибутов, приводят к просмотру карты атрибутов(cause the attribute map to be consulted). См. `Как работают атрибуты`_.
- Состояние прокручиваемых(scrollable) видопортов(viewports): состояние любых прокручиваемых видопортов(viewports) на *предыдущем(previous)* рисовании(drawing) будет рассмотрено. Для более деталей, см. `Видопорты(viewports)`_.

Функция ``appDraw`` вызывается, когда цикл события начинает рисовать приложение по мере его появления. Это(it) также вызывается сразу(right) после обработки события ``appHandleEvent``. Несмотря на то, что(Even though) функция возвращает спецификацию того, как рисовать весь экран, базовая(underlying) библиотека ``vty`` приводит к некоторым проблемам для эффективного обновления только тех частей экрана, которые изменились, поэтому вам не нужно беспокоиться об этом.

Где я найду функции рисования?
******************************

Наиболее важный модуль, обеспечивающий функции рисования есмь ``Brick.Widgets.Core``. Кроме того(Beyond that), любой модуль в пространстве имен ``Brick.Widgets`` предоставляет(provides) специфические виды функциональности.

appHandleEvent: Обработка событий(Handling Events)
--------------------------------------------------

Значение ``appHandleEvent`` есмь функция, которая решает, как модифицировать состояние приложения как(as) результат события:

.. code:: haskell

   appHandleEvent :: s -> BrickEvent n e -> EventM n (Next s)

Первый параметр типа ``s`` есмь состояние вашего приложения на момент(at the time) поступления(arrives) события. ``appHandleEvent`` отвечает(is responsible) за решение о том, как изменить состояние на основе события, а затем вернуть его.

Второй параметр типа ``BrickEvent n e`` есмь само(itself) событие.
Тип переменных ``n`` и ``e`` соответствуют(correspond) *типу имени рессурса(resource name type)* и *типу события(event type)* вашего приложения, соответственно(respectively), и должны соответствовать(match) соответствующим (corresponding) типам в ``App`` и ``EventM``.

Тип возвращаемого значения ``Next s`` (value) описывает, что должно произойти после завершения обработчика события. У нас есть три варианта:

* ``Brick.Main.continue s``: продолжить выполнение цикла событий с указанным(specified) состоянием приложения ``s`` в качестве следующего значения. Обычно здесь вы должны(this is where you'd) изменить состояние на основе события и вернуть его(return it).
* ``Brick.Main.halt s``: остановить(halt) цикл событий и вернуть(return) значение конечного(final) состояния приложения ``s``. Это значение состояния возвращается(is returned) вызывающей стороне(to the caller) ``defaultMain`` или ``customMain``  где его можно использовать до окончательного выхода из(it can be used prior to finally exiting) ``main``.
* ``Brick.Main.suspendAndResume act``: приостановить(suspend) цикл событий ``brick`` и выполнить указанное действие ``IO`` ``act``. Действие ``act`` должно быть типа ``IO s``, поэтому, когда оно выполняется, оно должно вернуть следующее состояние приложения.Когда используется ``suspendAndResume``, цикл событий ``brick`` завершается(shut down), и состояние терминала восстанавливается(restored) до состояния, когда цикл событий ``brick`` запускается(began execution). Когда он(it) закончит(finishes) выполнение(executing), цикл событий(event loop) будет возобновлен(will be resumed) используя(using) возвращаемое(returned) значение(value) состояния. Это есмь полезно для ситуаций, где ваша программа нуждается(needs) приостановить(to suspend) ваш интерфейс и выполнить(execute) некоторую(some) другую(other) программу, которая(that) нуждается, чтобы получить(to gain) контроль над терминалом(control of the terminal) (такой как внешний редактор).

Монада ``EventM`` - это монада обработки событий(event-handling). Эта монада является трансформером вокруг(around) ``IO`` поэтому(so) вы можете (are free) делать I/O в этой монаде используя ``liftIO``. Помимо  ввода-вывода(I/O), эта монада используется для прокрутки запросов к рендерингу(to make scrolling requests to the renderer) (см. `Viewports`_) и получения названных экстентов(obtain named extents) (см. `Extents`_).
Имейте в виду(Keep in mind), что время, затрачиваемое на блокирование в вашем обработчике событий, является временем, в течение которого ваш пользовательский интерфейс не отвечает, поэтому подумайте(consider) об этом, решив, использовать ли фоновые потоки, вместо того, чтобы встраивать работу в обработчик событий
(when deciding whether to have background threads do work instead of inlining the work in the event handler).

Обработчики событий Widget
**************************

Обработчики событий отвечают(responsible) за преобразование(transforming) состояния приложения.
Хотя(While) вы можете использовать обычные(ordinary) методы для этого(to do this), такие как сопоставление шаблонов(pattern matching) и вызовы функций(pure function calls), некоторые типы состояний виджетов, такие как те, которые(the ones) предоставляются(provided) модулями ``Brick.Widgets.List`` и ``Brick.Widgets.Edit`` предоставляют(provide) их собственные функции обработки событий, зависящие от виджета(widget-specific event-handling).
Для примера, ``Brick.Widgets.Edit`` предоставляет(provides) ``handleEditorEvent`` а(and) ``Brick.Widgets.List`` предоставляет(provides) ``handleListEvent``.

Поскольку(Since) эти обработчики событий запускаются(run) в ``EventM``, они имеют доступ к визуализации(rendering) состояний видопорта(viewport) через ``Brick.Main.lookupViewport`` и монада ``IO`` через ``liftIO``.

Чтобы использовать эти обработчики в вашей программе, вызовите(invoke) их в соответствующем(relevant) разделе(piece) состояния вашего приложения. В следующем примере мы используем состояние ``Edit`` из ``Brick.Widgets.Edit``:

.. code:: haskell

   data Name = Edit1
   type MyState = Editor String Name

   myEvent :: MyState -> BrickEvent n e -> EventM Name (Next MyState)
   myEvent s (VtyEvent e) = continue =<< handleEditorEvent e s

Этот шаблон(pattern) работает достаточно хорошо, когда ваше состояние приложения имеет обработчик событий, как показано выше, в примере ``Edit``, но может оказаться(become) неприятным(unpleasant), если значение, по которому вы хотите вызвать(invoke) обработчик, глубоко внедрено(embedded deeply) в(within) ваше состояние приложения. Если вы выбрали(have chosen) создание(generate) линз(lenses) для полей состояния вашего приложения, вы можете использовать удобную(convenience) функцию ``handleEventLensed`` указав(specifying) свое(your) состояние, линзы(lens), и события:

.. code:: haskell

   data Name = Edit1
   data MyState = MyState { _theEdit :: Editor String Name
                          }
   makeLenses ''MyState

   myEvent :: MyState -> BrickEvent n e -> EventM Name (Next MyState)
   myEvent s (VtyEvent e) = continue =<< handleEventLensed s theEdit handleEditorEvent e

Вы, может быть, сочтёте(might consider), что предпочтительнее(preferable) рассахаренная(desugared) версия:

.. code:: haskell

   myEvent :: MyState -> BrickEvent n e -> EventM Name (Next MyState)
   myEvent s (VtyEvent e) = do
     newVal <- handleEditorEvent e (s^.theEdit)
     continue $ s & theEdit .~ newVal

Использование собственного(Your Own) типа события
*************************************************

Поскольку(Since) нам(we) часто нужно связывать(to communicate) события, связанные с конкретным приложением(application-specific), вне(beyond) событий ввода Vty, к(to) обработчику событий, brick поддерживает встраивание(embedding) пользовательских(custom) событий вашего приложения в поток ``BrickEvent``-ов, который получит(receive) ваш обработчик. Тип этих событий есмь тип ``e`` упомянутый в ``BrickEvent n e`` и ``App s e n``.

Замечание(Note): как правило(ordinarily), ваше приложение не будет иметь свой собственный(its own custom) тип события, поэтому вы можете оставить этот тип неиспользуемым (e.g. ``App MyState e MyName``) или просто установите его на единицу(unit) (``App MyState () MyName``).

Ниже приведен(Здесь есмь(Here's)) пример использования настраиваемого(custom) типа событий. Предположим(Suppose), что вы хотите(you'd like) обрабатывать встречные события(to be able to handle counter events) в вашем обработчике событий. Сначала(First) мы определяем тип события счетчика(counter event type):

.. code:: haskell

   data CounterEvent = Counter Int

С этим объявлением типа мы можем теперь использовать события счетчика в нашем приложении(app), используя тип приложения ``App s CounterEvent n``. 
Для обработки этих событий мы будем просто нуждаться в поиске(need to look for) значений ``AppEvent`` в обработчике событий:

.. code:: haskell

   myEvent :: s -> BrickEvent n CounterEvent -> EventM n (Next s)
   myEvent s (AppEvent (CounterEvent i)) = ...

Следующий шаг есмь на самом деле(is to actually) *сгенерировать(generate)* наши пользовательские(custom) события и вставить(inject) их в поток событий ``brick``, чтобы они попали(so they make it) в обработчик событий. Для этого нам нужно создать(To do that we need to create a) ``BChan`` для наших пользовательских событий, предоставить это ``BChan`` в ``brick``, а затем отправить наши события по этому(over that) каналу. После того как мы создали канал с помощью(Once we've created the channel with) ``Brick.BChan.newBChan``, мы предоставляем его ``brick`` с ``customMain`` вместо ``defaultMain``:

.. code:: haskell

   main :: IO ()
   main = do
       eventChan <- Brick.BChan.newBChan 10
       finalState <- customMain
                       (Graphics.Vty.mkVty Data.Default.defaultConfig)
                       (Just eventChan) app initialState
       -- Use finalState and exit


Функция ``customMain`` позволяет нам контролировать, как библиотека ``vty`` инициализируется *и* как ``brick`` получает пользовательские(custom) события, чтобы предоставить(to give) нашему обработчику событий. ``customMain`` есмь точка входа в ``brick`` когда вам нужно использовать свой собственный тип события, как показано здесь.

При всём этом в месте(With all of this in place) отправка пользовательских событий в обработчик событий проста:

.. code:: haskell

   counterThread :: Brick.BChan.BChan CounterEvent -> IO ()
   counterThread chan = do
       Brick.BChan.writeBChan chan $ Counter 1

Ограниченные(Bounded) каналы
****************************

``BChan``, или *bounded channel*, могут содержать ограниченное количество элементов до того, как будут заблокированы попытки записи новых элементов. При(In) вызове ``newBChan`` (above), созданный(created) канал имеет емкость 10 элементов(items). 
Использование ограниченного канала гарантирует(ensures), что если программа не может обрабатывать(process) события достаточно быстро, тогда существует ограничение на то, сколько памяти будет использовано для хранения необработанных событий.
Таким образом(Thus), выбранная(chosen) емкость(capacity) должна быть достаточно большой, чтобы периодически накапливать всплески(to buffer occasional spikes) при задержке(latency) обработки событий без непреднамеренного блокирования произвольных производителей событий(inadvertently blocking custom event producers). Каждое приложение будет иметь свои собственные характеристики производительности, которые определяют(determine) наилучшую привязку(bound) для канала событий. В общем, рассмотрите производительность вашего обработчика событий при выборе ёмкости канала и разработчиков проектных событий(design event producers) так, чтобы они могли блокировать, если канал заполнен.

Начинаем(Starting up): appStartEvent
************************************

Когда приложение запускается, может быть желательно выполнить(perform) некоторые из обязательств(обязанностей(duties)), которые обычно возможны только после наступления события, такие как установка начального(initial) состояния видопорта с прокруткой(scrolling viewport). Поскольку такие действия могут быть выполнены(performed) только в ``EventM`` и поскольку мы не хотим ждать, пока первое событие прибудет(arrives), чтобы выполнить эту работу в ``appHandleEvent``, тип ``App`` предоставляет функцию ``appStartEvent``  для этого:

.. code:: haskell

   appStartEvent :: s -> EventM n s

Эта функция принимает начальное(initial) состояние приложения и возвращает его в ``EventM``, возможно, изменяя его и, возможно, делая запросы в видопорт.
Эта функция вызывается(is invoked) один раз и только один раз при запуске(startup) приложения.
Для более подробных деталей смотрите `Viewports`_. Вы, возможно, просто захотите использовать ``return`` как реализацию(implementation) этой функции для большинства приложений.

appChooseCursor: Размещение курсора(Placing the Cursor)
-------------------------------------------------------

Процесс воспроизведения(rendering) для ``Widget`` может возвращать информацию о том, где этот виджет хотел бы поместить(to place) курсор. Для примера, текстовый редактор должен будет(will need) сообщить(to report) позицию курсора(о позиции курсора(a cursor position)). Однако, так как(since) ``Widget`` может быть составной(composite) частью из многих таких виджетов с размещением курсора(cursor-placing widgets), мы должны(have)  иметь способ выбрать(way of choosing), какую(which) из сообщенных позиций курсора, если они есть(of the reported cursor positions, if any), мы на самом деле хотим соблюдать(is the one we actually want to honor).

Чтобы решить(To decide), какое размещение(placement) курсора использовать, или чтобы решить не показывать его вообще(one at all), мы устанавливаем(set) ``App`` (type's) функцию ``appChooseCursor``:

.. code:: haskell

   appChooseCursor :: s -> [CursorLocation n] -> Maybe (CursorLocation n)

Цикл событий воспроизводит(renders)  интерфейс и собирает(collects) значения ``Brick.Types.CursorLocation`` произведенные в процессе воспроизводения(rendering) и передает(passes) их вместе(along) с текущим состоянием приложения в эту функцию. Используя состояние вашего приложения (скажем, для отслеживания(to track) того, какое поле(box) ввода текста является "focused") вы можете решить(decide), в какое из мест(locations) вернуть или вернуть ``Nothing`` если вы не хотите отображать(to show) курсор.

Многие виджеты в процессе воспроизводения(rendering) могут запрашивать размещение курсора, но наше приложение должно определить, какой из них (если он есть) следует использовать(it is up to our application to determine which one (if any) should be used). Поскольку мы можем отображать в терминале только один курсор, нам нужно решить, какое место отображать(Since we can only show at most a single cursor in the terminal, we need to decide which location to show). Один из способов - посмотреть имя ресурса, содержащееся(One way is by looking at the resource name contained) в поле ``cursorLocationName``. Значение имени(The name value), связанное(associated) с положением(location) курсора, будет именем, используемым для запроса(to request) позиции курсора с помощью ``Brick.Widgets.Core.showCursor``.

  

``Brick.Main`` предоставляет(provides) различные(various) удобные(convenience) функции для упрощения выбора курсора в типичных случаях(to make cursor selection easy in common cases):

* ``neverShowCursor``: never show any cursor.
* ``showFirstCursor``: always show the first cursor request given; good
  for applications with only one cursor-placing widget.
* ``showCursorNamed``: show the cursor with the specified resource name
  or show no cursor if the name was not associated with any requested
  cursor position.

Для примера, этот виджет запрашивает(requests) размещение курсора на первой "``o``" в "``foo``" связанной(associated) с именем курсора ``CustomName``:

.. code:: haskell

   data MyName = CustomName

   let w = showCursor CustomName (Brick.Types.Location (1, 0))
             (Brick.Widgets.Core.str "foobar")



Обработчик событий для этого приложения будет использовать ``MyName`` в качестве (its) типа имени ресурса ``n`` и сможет сопоставить шаблон с(would be able to pattern-match on) ``CustomName``, чтобы соответствовать запросам курсора при воспроизведении этого виджета(to match cursor requests when this widget is rendered):

.. code:: haskell

   myApp = App { ...
               , appChooseCursor = \_ -> showCursorNamed CustomName
               }

See the next section for more information on using names.

Имена ресурсов
--------------

Мы видели выше в `appChooseCursor: Размещение курсора(Placing the Cursor)`_, что имена ресурсов используются для описания местоположений(locations) курсоров. Имена ресурсов также используются для обозначения других видов ресурсов:

* видопорты(см. `Видопорты`_)
* воспроизведение extent-ов (см. `Extent-ы`_)
* события мыши (см. `Поддержка мыши(Mouse Support)`_)

Назначение(Assigning) имен(names) этим типам ресурсов позволяет нам различать события на основе части интерфейса, с которой связано событие
to distinguish between events based on the part of the interface to which an event is related).

Ваше приложение должно указать имя определенного типа.
For simple applications that don't make use of resource names, you may use ``()``. But if your application has more than one named resource, you *must* provide a type capable of assigning a unique name to every resource that needs one.

Предостережение(A Note of Caution)
**********************************

Имена ресурсов могут быть назначены(assigned) любому из типов ресурсов, упомянутых выше, но некоторые рессурсы типы--видопорты(resource types--viewports), экстенты(степени)(extents), кэш воспроизведения(render cache), и курсор локаций--формы(cursor locations--form) отдельные пространства имен ресурсов(separate resource namespaces). Так, для примера, такое же(same) имя может быть назначено как видопорту, так и экстенту,  поскольку API ``brick`` предоставляет доступ к видопортам и экстентам с использованием отдельных(separate) API и структур данных. Однако, если такое же(same) имя используется для двух ресурсов одного и того же вида(kind), неизвестно(it is undefined), к *какому(which)* из них вы получите доступ, когда будете использовать(you go to use) один из этих ресурсов в своем обработчике событий.

Для примера, если одно и то же имя назначено двум видопортам:

.. code:: haskell

   data Name = Viewport1

   ui :: Widget Name
   ui = (viewport Viewport1 Vertical $ str "Foo") <+>
        (viewport Viewport1 Vertical $ str "Bar") <+>

затем в ``EventM``, когда мы пытаемся прокрутить окно просмотра ``Viewport1``, мы не знаем, какое из двух применений ``Viewport1`` будет затронуто:

.. code:: haskell

   do
     let vp = viewportScroll Viewport1
     vScrollBy vp 1

Решение состоит в том, чтобы гарантировать, что для данного типа ресурса (в данном случае видопорта) уникальное имя назначается при каждом использовании.

.. code:: haskell

   data Name = Viewport1 | Viewport2

   ui :: Widget Name
   ui = (viewport Viewport1 Vertical $ str "Foo") <+>
        (viewport Viewport2 Vertical $ str "Bar") <+>


appAttrMap: Управление атрибутами(Managing Attributes)
------------------------------------------------------

В ``brick`` мы используем *атрибутную карту(attribute map)* для назначения(to assign) атрибутов элементам интерфейса. Вместо указывания(Rather than specifying) специфических атрибутов при(when) рисовании виджета(например, текст красного на черном), мы указываем *имя атрибута*, которое является абстрактным именем для типа(kind) того, что(of thing) мы рисуем, например "ключевое слово" или "e-mail адрес."
Затем мы предоставляем карту атрибутов, которая сопоставляет эти имена атрибутов с фактическими(actual) атрибутами. Такой подход(approach) позволяет нам:


* Изменять атрибуты во время выполнения(runtime), позволяя пользователю произвольно(arbitrarily) изменять атрибуты любого элемента приложения, не заставляя кого-либо создавать специальное оборудование(machinery), чтобы сделать это конфигурируемым;
(Change the attributes at runtime, letting the user change the attributes of any element of the application arbitrarily without forcing anyone to build special machinery to make this configurable)
* Написать процедуры(routines) для загрузки(to load) сохраненных карт атрибутов с диска;
* Provide modular attribute behavior for third-party components, where we would not want to have to recompile third-party code just to change attributes, and where we would not want to have to pass in attribute arguments to third-party drawing functions.

Это позволяет нам разместить(put) отображение атрибутов(attribute mapping) для всего приложения(entire app), независимо(regardless) от использования сторонних(third-party) виджетов, в одном месте.

Для создания карты мы используем ``Brick.AttrMap.attrMap``, e.g.,

.. code:: haskell

   App { ...
       , appAttrMap = const $ attrMap Graphics.Vty.defAttr [(someAttrName, fg blue)]
       }

To use an attribute map, we specify the ``App`` field ``appAttrMap`` as the function to return the current attribute map each time rendering occurs. This function takes the current application state, so you may choose to store the attribute map in your application state. You may also choose not to bother with that and to just set ``appAttrMap = const someMap``.

To draw a widget using an attribute name in the map, use ``Brick.Widgets.Core.withAttr``. For example, this draws a string with a ``blue`` background:

.. code:: haskell

   let w = withAttr blueBg $ str "foobar"
       blueBg = attrName "blueBg"
       myMap = attrMap defAttr [ (blueBg, Brick.Util.bg Graphics.Vty.blue)
                               ]

For complete details on how attribute maps and attribute names work, see the Haddock documentation for the ``Brick.AttrMap`` module. See also `How Attributes Work`_.


Как работают Виджеты и Воспроизведения(rendering)
=================================================

Когда ``brick`` воспроизводит(renders) ``Widget``,
процедура воспроизведения(rendering) виджета оценивается для создания
``vty`` ``Image`` виджета(widget).
Процедура воспроизведения(rendering) виджета выполняется с некоторой информацией, называемой *контекстом воспроизведения(rendering context*, который содержит:

* Размер области, в которой можно рисовать вещи(things)
* Имя текущего атрибута для рисования вещей(things)
* Карта атрибутов, используемых для поиска имен атрибутов
* Активный стиль границы(border), используемый при рисовании границ(borders)

Доступная область воспроизведения(Available Rendering Area)
-----------------------------------------------------------

Наиболее важным элементом контекста воспроизведения(rendering) является область воспроизведения(rendering): Эта часть контекста говорит, что виджет отображает, сколько строк и столбцов доступно для него. Когда рендеринг начинается, визуализируемый виджет (то есть слой, возвращаемый функцией ``appDraw``) получает контекст воспроизведения(rendering), область воспроизведения(rendering) которого является размером терминала. Эта информация о размере используется, чтобы позволить виджетам занимать это пространство, если они этого захотят. Например, строка «Hello, world!» всегда будет занимать одну строку и 13 столбцов, но строка "Hello, world!" *centered* всегда будет занимать одну строку и *все доступные столбцы*.

Как виджеты используют пространство при визуализации, описывается в двух частях информации в каждом ``Widget``: политики горизонтального и вертикального роста виджета(growth policies). Эти поля имеют тип ``Brick.Types.Size`` и могут иметь значения ``Fixed`` и ``Greedy``. Обратите внимание, что эти значения являются всего лишь *описательными подсказками(descriptive hints* о поведении функции воспроизведения(rendering), поэтому важно, чтобы они точно описывали использование виджета в пространстве.

Виджет, рекламирующий размер ``Fixed`` в заданном измерении, - это виджет, который всегда будет потреблять одинаковое количество строк или столбцов, независимо от того, сколько он задан. Виджеты могут рекламировать различные вертикальные и горизонтальные политики роста, например, функция ``Brick.Widgets.Border.hCenter`` центрирует виджет и является ``Greedy`` горизонтально и отбрасывает виджет, который он центрирует для вертикального роста.

Эти политики размера определяют алгоритм компоновки ящиков, который лежит в основе каждой нетривиальной спецификации чертежа. Когда мы используем ``Brick.Widgets.Core.vBox`` и ``Brick.Widgets.Core.hBox``, чтобы проложить (или использовать их бинарные синонимы ``<=>`` и ``<+>``, соответственно), алгоритм компоновки ящиков рассматривает политики роста виджетов, которые он получает, чтобы определить, как распределить доступное пространство для них.

Например, представьте, что в настоящее время оконечное окно имеет 10 рядов и 50 колонок. Мы хотим отобразить следующий виджет:

.. code:: haskell

   let w = (str "Hello," <=> str "World!")

Воспроизведение(Rendering) этого в терминал приведет(result) к "Hello" и "World!" под ним, с 8 рядами(rows), ничем не занятыми(unoccupied by anything). Но если бы мы хотели визуализировать вертикальную границу под этими строками, мы бы написали:
But if we wished to render a vertical border underneath those strings, we would write:

.. code:: haskell

   let w = (str "Hello," <=> str "World!" <=> vBorder)

Воспроизведение(Rendering) этого в терминал приведет(result) к "Hello" и "World!" под ним, с 8 рядами(rows), оставшихся занятыми символами вертикальной границы(remaining occupied by vertical border characters) ("``|``") шириной(wide) в один столбец(column). Виджет с вертикальной границей(border) предназначен(is designed) для того, чтобы занимать столько строк, сколько ему было задано(to take up however many rows it was given), но воспроизведение(rendering) алгоритма компоновки блока должен быть осторожен(the box layout algorithm has to be careful) при(about) отображении(rendering)  таких виджетов ``Greedy``, потому что они не оставят(won't leave) места(room) для чего-либо(anything) еще. Поскольку виджет-бокс(Since the box widget) не может знать размеры своих подвиджетов(sub-widgets), пока(until) они не будут отрисованы(they are rendered), виджеты ``Fixed`` воспроизводятся(get rendered), и их размеры используются для определения(determine) того, сколько места(space) осталось (left) для виджетов ``Greedy``.

При(When) использовании виджетов важно понимать их горизонтальное и вертикальное поведение в пространстве, зная их значения ``Size``. Это должно быть ясно указано(made clear) в документации Haddock(Хэддок?).

Спецификация контекста воспроизведения(rendering) доступного пространства(available space) также будет определять способ обрезки виджетов, поскольку все виджеты должны отображаться на изображении размером не больше(govern how widgets get cropped, since all widgets are required to render to an image no larger than) указанного(specifies) контекста воспроизведения(rendering). Если они это сделают, они будут насильственно(forcibly) обрезаны(cropped).

Limiting Rendering Area
-----------------------

Если вы хотите использовать виджет ``Greedy`` но хотите ограничить объем занимаемого им пространства, вы можете превратить(turn) его в  виджет ``Fixed`` используя один из *ограничивающих комбинаторов(limiting combinators)*, ``Brick.Widgets.Core.hLimit`` и ``Brick.Widgets.Core.vLimit``. Эти комбинаторы берут виджеты и превращают их в виджеты с размером ``Fixed`` (в соответствующем(relevant) измерении) и запускают свои функции воспроизведения(rendering) в модифицированном контексте воспроизведения(rendering) с ограниченной областью воспроизведения(rendering).
(and run their rendering functions in a modified rendering context with a restricted rendering area)

Для примера, следующее будет центрировать строку в 30 столбцах, оставляя место для чего-то, что будет размещено рядом с ним при изменении ширины терминала
(the following will center a string in 30 columns, leaving room for something to be placed next to it as the terminal width changes):

.. code:: haskell

   let w = hLimit 30 $ hCenter $ str "Hello, world!"

The Attribute Map
-----------------

The rendering context contains an attribute map (see `How Attributes
Work`_ and `appAttrMap: Managing Attributes`_) which is used to look up
attribute names from the drawing specification. The map originates from
``Brick.Main.appAttrMap`` and can be manipulated on a per-widget basis
using ``Brick.Widgets.Core.updateAttrMap``.

Активный стиль границы
----------------------

Виджеты в модуле ``Brick.Widgets.Border`` рисуют границу символов (characters) (horizontal, vertical, and boxes) между и вокруг других виджетов. To ensure that widgets across your application share a consistent visual style, border widgets consult the rendering context's *active border style*, a value of type ``Brick.Widgets.Border.Style``, to get the characters used to draw borders.

The default border style is ``Brick.Widgets.Border.Style.unicode``. To
change border styles, use the ``Brick.Widgets.Core.withBorderStyle``
combinator to wrap a widget and change the border style it uses when
rendering. For example, this will use the ``ascii`` border style instead
of ``unicode``:

.. code:: haskell

   let w = withBorderStyle Brick.Widgets.Border.Style.ascii $
             Brick.Widgets.Border.border $ str "Hello, world!"

By default, borders in adjacent widgets do not connect to each other.
This can lead to visual oddities, for example, when horizontal borders
are drawn next to vertical borders by leaving a small gap like this:

.. code:: text

    │─

You can request that adjacent borders connect to each other with
``Brick.Widgets.Core.joinBorders``. Two borders drawn with the
same attribute and border style, and both under the influence of
``joinBorders``, will produce a border like this instead:

.. code:: text

    ├─

See `Joining Borders`_ for further details.

How Attributes Work
===================

In addition to letting us map names to attributes, attribute maps
provide hierarchical attribute inheritance: a more specific attribute
derives any properties (e.g. background color) that it does not specify
from more general attributes in hierarchical relationship to it, letting
us customize only the parts of attributes that we want to change without
having to repeat ourselves.

For example, this draws a string with a foreground color of ``white`` on
a background color of ``blue``:

.. code:: haskell

   let w = withAttr specificAttr $ str "foobar"
       generalAttr = attrName "general"
       specificAttr = attrName "general" <> attrName "specific"
       myMap = attrMap defAttr [ (generalAttr, bg blue)
                               , (specificAttr, fg white)
                               ]

Functions ``Brick.Util.fg`` and ``Brick.Util.bg`` specify
partial attributes, and map lookups start with the desired name
(``general/specific`` in this case) and walk up the name hierarchy (to
``general``), merging partial attribute settings as they go, letting
already-specified attribute settings take precedence. Finally, any
attribute settings not specified by map lookups fall back to the map's
*default attribute*, specified above as ``Graphics.Vty.defAttr``. In
this way, if you want everything in your application to have a ``blue``
background color, you only need to specify it *once*: in the attribute
map's default attribute. Any other attribute names can merely customize
the foreground color.

In addition to using the attribute map provided by ``appAttrMap``,
the map can be customized on a per-widget basis by using the attribute
map combinators:

* ``Brick.Widgets.Core.updateAttrMap``
* ``Brick.Widgets.Core.forceAttr``
* ``Brick.Widgets.Core.withDefAttr``
* ``Brick.Widgets.Core.overrideAttr``

Attribute Themes
================

Brick provides support for customizable attribute themes. This works as
follows:

* The application provides a default theme built in to the program.
* The application customizes the them by loading theme customizations
  from a user-specified customization file.
* The application can save new customizations to files for later
  re-loading.

Customizations are written in an INI-style file. Here's an example:

.. code:: ini

   [default]
   default.fg = blue
   default.bg = black

   [other]
   someAttribute.fg = red
   someAttribute.style = underline
   otherAttribute.style = [underline, bold]
   otherAttribute.inner.fg = white

In the above example, the theme's *default attribute* -- the one that is
used when no other attributes are used -- is customized. Its foreground
and background colors are set. Then, other attributes specified by
the theme -- ``someAttribute`` and ``otherAttribute`` -- are also
customized. This example shows that styles can be customized, too, and
that a custom style can either be a single style (in this example,
``underline``) or a collection of styles to be applied simultaneously
(in this example, ``underline`` and ``bold``). Lastly, the hierarchical
attribute name ``otherAttribute.inner`` refers to an attribute name
with two components, ``otherAttribute <> inner``, similar to the
``specificAttr`` attribute described in `How Attributes Work`_. Full
documentation for the format of theme customization files can be found
in the module documentation for ``Brick.Themes``.

The above example can be used in a ``brick`` application as follows.
First, the application provides a default theme:

.. code:: haskell

   import Brick.Themes (Theme, newTheme)
   import Brick (attrName)
   import Brick.Util (fg, on)
   import Graphics.Vty (defAttr, white, blue, yellow, magenta)

   defaultTheme :: Theme
   defaultTheme =
       newTheme (white `on` blue)
                [ (attrName "someAttribute",  fg yellow)
                , (attrName "otherAttribute", fg magenta)
                ]

Notice that the attributes in the theme have defaults: ``someAttribute``
will default to a yellow foreground color if it is not customized. (And
its background will default to the theme's default background color,
blue, if it not customized either.) Then, the application can customize
the theme with the user's customization file:

.. code:: haskell

   import Brick.Themes (loadCustomizations)

   main :: IO ()
   main = do
       customizedTheme <- loadCustomizations "custom.ini" defaultTheme

Now we have a customized theme based on ``defaultTheme``. The next step
is to build an ``AttrMap`` from the theme:

.. code:: haskell

   import Brick.Themes (themeToAttrMap)

   main :: IO ()
   main = do
       customizedTheme <- loadCustomizations "custom.ini" defaultTheme
       let mapping = themeToAttrMap customizedTheme

The resulting ``AttrMap`` can then be returned by ``appAttrMap``
as described in `How Attributes Work`_ and `appAttrMap: Managing
Attributes`_.

If the theme is further customized at runtime, any changes can be saved with ``Brick.Themes.saveCustomizations``.

Поддержка широких символов(Wide Character) и класс TextWidth
============================================================

Brick поддерживает рендеринг(rendering) широких символов во всех виджетах, а редактор brick поддерживает ввод и редактирование широких символов. Широкие символы - это такие, как многие азиатские символы и смайлики, для отображения которых требуется более одного столбца терминала(are those such as many Asian characters and emoji that need more than a single terminal column to be displayed). Brick полагается(relies) на использование Vty библиотеки `utf8proc`_ для определения(determine) ширины столбца каждого отображаемого(rendered) символа.

As a result of supporting wide characters, it is important to know that computing the length of a string to determine its screen width will *only* work for single-column characters. So, for example, if you want to support wide characters in your application, this will not work:

.. code:: haskell

   let width = Data.Text.length t

because if the string contains any wide characters, their widths
will not be counted properly. In order to get this right, use the
``TextWidth`` type class to compute the width:

.. code:: haskell

   let width = Brick.Widgets.Core.textWidth t

Тип класса ``TextWidth`` использует Vty процедуру(routine) ширины символа(character) (and thus ``utf8proc``), чтобы вычислить правильную ширину. If you need to compute the width of a single character, use ``Graphics.Text.wcwidth``.

Extent-ы
========

Когда приложению необходимо знать, где конкретный(a particular) виджет был отрисован воспроизводителем(by the renderer),  приложение может запросить(request), чтобы воспроизводитель записал *extent* виджета--его верхне-левый(upper-left) угол и размер--и предоставило(provide) его в обработчике событий.
В следующем примере приложению необходимо знать, где воспроизводится(is rendered) поле с рамкой(bordered box), содержащее "Foo":

.. code:: haskell

   ui = center $ border $ str "Foo"

Мы не хотим заботиться о деталях макета, чтобы выяснить, где находится рамка с рамкой во время рендеринга(to have to care about the particulars of the layout to find out where the bordered box got placed during rendering). Чтобы получить эту информацию, мы просим(request), чтобы модуль рендеринга сообщил нам размер окна(that the extent of the box be reported to us by the renderer), используя имя ресурса:

.. code:: haskell

   data Name = FooBox

   ui = center $
        reportExtent FooBox $
        border $ str "Foo"

Теперь всякий раз, когда(whenever) воспроизводится(is rendered) ``ui``, расположение(location) и размер поля с рамкой(bordered box), содержащего "Foo", будут записываться. Мы можем затем найти его в обработчиках событий в EventM:
 the location and size of the bordered box containing "Foo" will be recorded. We can then look it up in event handlers in ``EventM``):

.. code:: haskell

   do
     mExtent <- Brick.Main.lookupExtent FooBox
     case mExtent of
       Nothing -> ...
       Just (Extent _ upperLeft (width, height) offset) -> ...

Поддержка Вставки
=================

Some terminal emulators support "bracketed paste" support. This feature
enables OS-level paste operations to send the pasted content as a
single chunk of data and bypass the usual input processing that the
application does. This enables more secure handling of pasted data since
the application can detect that a paste occurred and avoid processing
the pasted data as ordinary keyboard input. For more information, see
`bracketed paste mode`_.

The Vty library used by brick provides support for bracketed pastes, but
this mode must be enabled. To enable paste mode, we need to get access
to the Vty library handle in ``EventM`` (in e.g. ``appHandleEvent``):

.. code:: haskell

   import Control.Monad (when)
   import qualified Graphics.Vty as V

   do
     vty <- Brick.Main.getVtyHandle
     let output = V.outputIface vty
     when (V.supportsMode output V.BracketedPaste) $
         liftIO $ V.setMode output V.BracketedPaste True

Once enabled, paste mode will generate Vty ``EvPaste`` events. These
events will give you the entire pasted content as a ``ByteString`` which
you must decode yourself if, for example, you expect it to contain UTF-8
text data.

Поддержка мыши
==============

Некоторые эмуляторы терминала поддерживают взаимодействие с мышью.
Библиотека Vty, используемая brick, предоставляет(provides) эти низкоуровневые события, если включен режим мыши(these low-level events if mouse mode has been enabled)
Чтобы включить(enable) режим(mode) мыши, нам нужно получить доступ к обработчику библиотеки Vty в ``EventM``:
.. code:: haskell

   do
     vty <- Brick.Main.getVtyHandle
     let output = outputIface vty
     when (supportsMode output Mouse) $
       liftIO $ setMode output Mouse True

Имейте в виду(Bear in mind), что некоторые терминалы не поддерживают взаимодействие с мышью(mouse interaction), поэтому(so) используйте Vty ``getModeStatus``, чтобы выяснить(to find out), будет ли ваш терминал(whether your terminal will) предоставлять(provide) события мыши.

Также имейте в виду, что пользователи терминала обычно ожидают, что смогут полностью взаимодействовать с вашим приложением без мыши, поэтому, если вы все же решите включить взаимодействие с мышью, подумайте об использовании его для улучшения существующих взаимодействий, а не для предоставления новых функций, которыми уже нельзя(to be ableto interact with your application entirely without a mouse, so if you do choose to enable mouse interaction, consider using it to improve existing interactions rather than provide new functionality that cannot already be) управлять с клавиатурой.

Низко-уровневые события мыши(Low-level Mouse Events
---------------------------------------------------

После включения событий мыши(Once mouse events have been enabled) Vty будет генерировать события ``EvMouseDown`` и ``EvMouseUp``, содержащие нажатую(clicked) кнопку мыши, местоположение(location) в терминале и любые нажатые клавиши-модификаторы(any modifier keys pressed).

.. code:: haskell

   handleEvent s (VtyEvent (EvMouseDown col row button mods) = ...

Brick Mouse Events
------------------

Хотя этих событий может хватить для ваших нужд, ``brick`` предоставляет(provides) высоко-уровневный(higher-level) интерфейс событий мыши, связанный(that ties) с языком рисования. Недостатком(disadvantage) низкоуровневого интерфейса, описанного выше, является то, что вам все равно нужно определить, *что* было нажато, то есть часть интерфейса, которая находилась под курсором мыши(to the low-level interface described above is that you still need to determine *what* was clicked, i.e., the part of the interface that was under the mouse cursor). Есть два способа сделать это с помощью ``brick``: с *extent checking(проверкой экстента)* и *отчетом по клику(click reporting)*.

Extent checking
***************

The *extent checking* approach entails requesting extents (see
`Extents`_) for parts of your interface, then checking the Vty mouse
click event's coordinates against one or more extents.

The most direct way to do this is to check a specific extent:

.. code:: haskell

   handleEvent s (VtyEvent (EvMouseDown col row _ _)) = do
     mExtent <- lookupExtent SomeExtent
     case mExtent of
       Nothing -> continue s
       Just e -> do
         if Brick.Main.clickedExtent (col, row) e
           then ...
           else ...

This approach works well enough if you know which extent you're
interested in checking, but what if there are many extents and you
want to know which one was clicked? And what if those extents are in
different layers? The next approach is to find all clicked extents:

.. code:: haskell

   handleEvent s (VtyEvent (EvMouseDown col row _ _)) = do
     extents <- Brick.Main.findClickedExtents (col, row)
     -- Then check to see if a specific extent is in the list, or just
     -- take the first one in the list.

This approach finds all clicked extents and returns them in a list with
the following properties:

* For extents ``A`` and ``B``, if ``A``'s layer is higher than ``B``'s
  layer, ``A`` comes before ``B`` in the list.
* For extents ``A`` and ``B``, if ``A`` and ``B`` are in the same layer
  and ``A`` is contained within ``B``, ``A`` comes before ``B`` in the
  list.

As a result, the extents are ordered in a natural way, starting with the
most specific extents and proceeding to the most general.

Click reporting
***************

*click reporting* approach is the most high-level approach
offered by ``brick``. When rendering the interface we use
``Brick.Widgets.Core.clickable`` to request that a given widget generate
``MouseDown`` and ``MouseUp`` events when it is clicked.

.. code:: haskell

   data Name = MyButton

   ui :: Widget Name
   ui = center $
        clickable MyButton $
        border $
        str "Click me"

   handleEvent s (MouseDown MyButton button modifiers coords) = ...
   handleEvent s (MouseUp MyButton button coords) = ...

This approach enables event handlers to use pattern matching to check
for mouse clicks on specific regions; this uses extent reporting
under the hood but makes it possible to denote which widgets are
clickable in the interface description. The event's click coordinates
are local to the widget being clicked. In the above example, a click
on the upper-left corner of the border would result in coordinates of
``(0,0)``.

Видопорты(Viewports)
====================

*Видопорт(viewport* есмь прокручиваемое окно на виджет.
Видопорты имеют *направление прокрутки(scrolling direction* типа ``Brick.Types.ViewportType`` которое может быть одним из:

* ``Horizontal``: видопорт может прокручиваться только горизонтально.
* ``Vertical``: видопорт может прокручиваться только вертикально.
* ``Both``: видопорт может прокручиваться и горизонтально и вертикально.

Комбинатор ``Brick.Widgets.Core.viewport`` берет(takes) другой(another) виджет и встраивает(embeds) его в именованный видопорт. Мы называем(именуем(name)) видопорт так, чтобы мы могли отслеживать его состояние прокрутки в рендерере, и чтобы вы могли делать запросы прокрутки(so that we can keep track of its scrolling state in the renderer, and so that you can make scrolling requests. Имя видопорта есмь это обработчик(its handle) для этих операций (см. `Прокрутка Видопортов в обработчиках событий(Scrolling Viewports in Event Handlers`_ и `Имена ресурсов(Resource Names`_). **Имя видопорта должно быть уникальным для(across) вашего приложения.**

Например, the following puts a string in a horizontally-scrollable viewport:

.. code:: haskell

   -- Assuming that App uses 'Name' for its resource names:
   data Name = Viewport1
   let w = viewport Viewport1 Horizontal $ str "Hello, world!"


Спецификация ``viewport`` означает, что виджет в области просмотра будет помещен в окно области просмотра, которое является ``Жадное(Greedy`` в обоих направлениях (см. `Доступная область воспроизведения(Available Rendering Area`_). Это подходит, если мы хотим, чтобы размер области просмотра был размером всего(entire) окна терминала, но если мы хотим ограничить размер области просмотра, мы могли бы использовать ограничивающие комбинаторы (см. `Ограничение области рендеринга`_):


.. code:: haskell

   let w = hLimit 5 $
           vLimit 1 $
           viewport Viewport1 Horizontal $ str "Hello, world!"

Теперь в примере создается прокручиваемое окно в одну строку в высоту и пять столбцов в ширину, изначально показывающее (example produces a scrollable window one row high and five columns wide initially showing) "Hello". В следующих двух разделах обсуждаются два способа прокрутки этого окна просмотра(sections discuss the two ways in which this viewport can be scrolled)

Формы ввода
===========

Хотя можно построить(construct) интерфейсы с редакторами и другими интерактивными ресурсами вручную, этот процесс несколько утомительный: вся диспетчеризации события(event dispatching) должно быть написано вручную, кольцо фокуса(focus ring) или другая конструкция необходимо управлять, и большинство потребностей коды воспроизведения(rendering) должны быть записаны(other construct needs to be managed, and most of the rendering code needs to be written). Кроме того, этот процесс делает его трудно следовать некоторым общим закономерностям(patterns):

* Как правило, мы хотим(We typically want), чтобы подтвердить ввод пользователя, и только собрать ее, когда она была подтверждена. to validate the user's input, and only collect it once it has been validated.)
* Как правило, мы хотим, чтобы уведомить пользователя, когда содержимое определенного поля являются недействительными(particular field's contents are invalid).
* Часто бывает полезно, чтобы иметь возможность создать новый тип данных для представления полого ввода интерфейса, и использовать его для инициализации элементов ввода, а затем собрать (валидацию) результатов
(It is often helpful to be able to create a new data type to represent the fields in an input interface, and use it to initialize the input elements and later collect the (validated) results).
* Много(A lot of) воспроизведения(rendering) и обработки событий работы предстоит сделать повторяющийся(work to be done is repetitive).

Модуль ``Brick.Forms`` обеспечивает(provides) высокоуровневый API для автоматизации всех вышеперечисленных работ в манере типобезопасного(above work in a type-safe manner).

Пример формы
------------

Давайте посмотрим пример типа данных, который мы хотим использовать как базис для интерфейса ввода. Этот пример взят непосредственно из демонстрационной программы ``FormDemo.hs``.

.. code:: haskell

   data UserInfo =
       FormState { _name      :: T.Text
                 , _age       :: Int
                 , _address   :: T.Text
                 , _ridesBike :: Bool
                 , _handed    :: Handedness
                 , _password  :: T.Text
                 } deriving (Show)

   data Handedness = LeftHanded
                   | RightHanded
                   | Ambidextrous
                   deriving (Show, Eq)


Предположим, что мы хотим построить(build) форму ввода для вышеупомянутых данных. Мы могли бы использовать редактор, чтобы позволить пользователю ввести имя и возраст. Мы должны убедиться, что возраст пользователя является допустимым для ввода целого. Для ```_ridesBike`` мы могли бы вход флажок стиля и ``_handed`` мы могли бы вход кнопки радио. Для ``_password``, мы определенно как поле ввода пароля, который прикрывает(conceals) ввод.
(We might want to use an editor to allow the user to enter a name and an age. We'll need to ensure that the user's input for age is a valid integer. For ``_ridesBike`` we might want a checkbox-style input, and for ``_handed`` we might want a radio button input. For ``_password``, we'd definitely like a password input box)

Если бы мы должны были построить интерфейс для этих данных вручную, мы должны были бы иметь дело с преобразованием данных выше нужных типов для входов.
(If we were to build an interface for this data manually, we'd need to deal with converting the data above to the right types for inputs.)
Например, для ``_age`` мы должны преобразовать исходное(initial) значение age в ``Text``, поставить его в редакторе с помощью ``Brick.Widgets.Edit.editor``, а затем в более позднее время, разобрать и восстановить значение или возраст от содержимого редактора.
(put it in an editor with ``Brick.Widgets.Edit.editor``, and then at a later time, parse the value and reconstruct an age from the editor's contents.)
Мы также должны сообщить пользователю, если значение возраста неверно.

API ``Форм(Forms)`` brick предоставляет входные(input) типы полей(field) для всех перечисленных выше вариантов использования(use cases). Вот форма, которую мы можем использовать, чтобы позволить пользователю редактировать значение ``UserInfo``:

.. code:: haskell

   mkForm :: UserInfo -> Form UserInfo e Name
   mkForm =
       newForm [ editTextField name NameField (Just 1)
               , editTextField address AddressField (Just 3)
               , editShowableField age AgeField
               , editPasswordField password PasswordField
               , radioField handed [ (LeftHanded, LeftHandField, "Left")
                                   , (RightHanded, RightHandField, "Right")
                                   , (Ambidextrous, AmbiField, "Both")
                                   ]
               , checkboxField ridesBike BikeField "Do you ride a bicycle?"
               ]

Форма представляется с использованием значения ``Form s e n`` и параметризуется с некоторыми типами:

* ``s`` - тип *form state* управляемый формой(managed by the form) (в данном случае ``UserInfo``)
* ``e`` - тип события приложения (должен соответствовать типу события, используемым с(used with) ``App``)
* ``n`` - тип имени ресурса приложения (должен совпадать с типом имени ресурса, используемым с(used with) ``App``)

Прежде всего(First of all), в приведенном выше коде предполагается, что мы получили(have derived) линзы для ``UserInfo``, используя ``Lens.Micro.TH.makeLenses``. Как только мы это сделаем, каждое поле, которое мы укажем в форме, должно предоставить линзу в ``UserInfo``, чтобы мы могли объявить конкретное поле ``UserInfo``, которое будет отредактировано этим полем. Например, для редактирования поля `` _name`` мы используем объектив `` name`` для создания редактора текстового поля с `` editTextField``. Все описанные выше конструкторы полей предоставляются ``Brick.Forms``.
(Once we've done that, each field that we specify in the form must provide a lens into ``UserInfo`` so that we can declare the particular field of ``UserInfo`` that will be edited by the field. For example, to edit the ``_name`` field we use the ``name`` lens to create a text field editor with ``editTextField``. All of the field constructors above are provided by ``Brick.Forms``.)

Каждому полю формы также требуется имя ресурса (см. `Имена ресурсов(Resource Names)`_). Имена ресурсов назначаются отдельным формам ввода, поэтому форма может автоматически отслеживать фокус ввода и обрабатывать события щелчка мыши.
(are assigned to the individual form inputs so the form can automatically track input focus and handle mouse click events.)

Форма несет с собой значение ``UserInfo``, которое отражает содержимое формы. Всякий раз, когда поле ввода в форме обрабатывает событие, его содержимое проверяется и переписывается в состояние формы (в этом случае - запись ``UserInfo``).

Функция ``mkForm``  принимает значение ``UserInfo``, что на самом деле является просто(which is really just) аргументом ``newForm``. Это значение ``UserInfo`` будет использоваться для инициализации всех полей формы.
Каждое поле формы будет использовать линзу предоставленную для извлечения начального значения из записи ``UserInfo``, преобразовать его в соответствующий тип состояния для рассматриваемого поля, а затем проверить это состояние и преобразовать его обратно в соответствующий тип для хранения в `` UserInfo``.
(Each form field will use the lens provided to extract the initial value from the ``UserInfo`` record, convert it into an appropriate state type for the field in question, and later validate that state and convert it back into the approprate type for storage in ``UserInfo``.)

Например, если исходное(initial) значение ``UserInfo`` поля ``_age`` имеет значение ``0``, ``editShowableField`` будет вызывать ``show`` на ``0``, преобразовывать его в ``Text`` и инициализировать редактор для ``_age`` текстовой строкой ``"0"``. Позже, если пользователь вводит больше текста -- изменяя содержимое редактора на ``"10"``, скажем -- экземпляр ``Read`` для ``Int`` (тип ``_age``) будет использоваться для разбора(parse) ``"10"``. Успешно разобранное(successfully-parsed) значение ``10`` будет записано в поле ``_age`` состояния формы ``UserInfo`` с использованием линзы ``age``. Использование `` Show`` и `` Read`` здесь является особенностью типа поля, который мы выбрали для `` _age``, `` editShowableField``.
(The use of ``Show`` and ``Read`` here is a feature of the field type we have chosen for ``_age``, ``editShowableField``.)

Для других типов полей у нас могут быть другие потребности. Например, ``Handedness`` есмь тип данных, представляющий все возможные варианты(choices), которые мы хотим предоставить для обеспечения готовности пользователя(user's handedness). Мы не хотим, чтобы пользователь вводил текстовую строку для этой опции. Более подходящим интерфейсом ввода является список переключателей для выбора среди доступных опций. Для этого у нас есть ``radioField``. Этот конструктор полей отображает список всех доступных параметров и обновляет состояние формы со значением выбранного в данный момент параметра.
(We wouldn't want the user to have to type in a text string for this option. A more appropriate input interface is a list of radio buttons to choose from amongst the available options. For that we have ``radioField``. This field constructor takes a list of all of the available options, and updates the form state with the value of the currently-selected option.)

Воспроизведение(Rendering) форм
----------------------------

Воспроизведение(Rendering) форм выполняется легко с помощью функции ``Brick.Forms.renderForm``. Однако, как написано выше, форма не будет выглядеть особенно красиво. Мы увидим несколько текстовых редакторов, за которыми следуют несколько переключателей(radio buttons) и флажок(check box). Но нам нужно немного(bit) настроить(customize) вывод, чтобы облегчить использование формы. Для этого у нас есть оператор ``Brick.Forms.@@=``. Этот оператор позволяет нам предоставить функцию для добавления(augment) ``Widget``, генерируемого функцией воспроизведения(rendering) поля, чтобы мы могли делать такие вещи, как добавление меток, макета элемента управления или изменение атрибутов:
generated by the field's rendering function so we can do things like add labels, control layout, or change attributes:

.. code:: haskell

    (str "Name: " <+>) @@=
      editTextField name NameField (Just 1)

Теперь, когда мы вызовем ``renderForm`` в форме, используя приведенный выше пример, мы увидим метку ``"Name:"`` слева от поля редактора для поля ``_name`` ``UserInfo``.
    
Brick предоставляет этот интерфейс для управления визуализацией для каждого поля, потому что многие поля формы либо не будут иметь меток, либо будут иметь разные макетные(layout) требования, поэтому альтернативный API, такой как встраивание метки в API полей, не всегда имеет смысл.

Brick по умолчанию отображает входные данные отдельных полей и всю форму в вертикальном поле с помощью  ``vBox``. Используйте 
``setFormConcat`` и ``setFieldConcat``, чтобы изменить это поведение, например, ``hBox``.

Обработка событий формы
-----------------------

Обработка событий формы проста: мы просто вызываем ``Brick.Forms.handleFormEvent`` с ``BrickEvent`` и ``Form``. Это автоматически отправляет(dispatches) события ввода в текущее(currently-focused) поле ввода, а также управляет изменениями фокуса с помощью клавиш(keybindings) ``Tab`` и ``Shift-Tab``. (Для деталей обо всех его поведениях см. Haddock-документацию  для ``handleFormEvent``.) Это все еще зависит от приложения, чтобы решить, когда события должны идти в форму в первую очередь(place).
It's still up to the application to decide when events should go to the form in the first place.

Since the form field handlers take ``BrickEvent`` values, that means that custom fields could even handle application-specific events (of the type ``e`` above).

Once the application has decided that the user should be done with the form editing session, the current state of the form can be obtained with ``Brick.Forms.formState``. In the example above, this would return a ``UserInfo`` record containing the values for each field in the form *as of the last time it was valid input*. This means that the user might have provided invalid input to a form field that is not reflected in the form state due to failing validation.

Since the ``formState`` is always a valid set of values, it might be surprising to the user if the values used do not match the last values they saw on the screen; the ``Brick.Forms.allFieldsValid`` can be used to determine if the last visual state of the form had any invalid entries and doesn't match the value of ``formState``. A list of any fields which had invalid values can be retrieved with the ``Brick.Forms.invalidFields`` function.

While each form field type provides a validator function to validate its current user input value, that function is pure. As a result it's not suitable for doing validation that requires I/O such as searching a database or making network requests. If your application requires that kind of validation, you can use the ``Brick.Forms.setFieldValid`` function to set the validation state of any form field as you see fit. The validation state set by that function will be considered by ``allFieldsValid`` and ``invalidFields``. See ``FormDemo.hs`` for an example of this API.

Note that if mouse events are enabled in your application (see `Mouse Support`_), all built-in form fields will respond to mouse interaction. Radio buttons and check boxes change selection on mouse clicks and editors change cursor position on mouse clicks.

Writing Custom Form Field Types
-------------------------------

If the built-in form field types don't meet your needs, ``Brick.Forms`` exposes all of the data types needed to implement your own field types. For more details on how to do this, see the Haddock documentation for the ``FormFieldState`` and ``FormField`` data types along with the implementations of the built-in form field types.

.. code:: haskell 

   let w = cropToContext someWidget


.. _vty: https://github.com/jtdaugherty/vty
.. _Hackage: http://hackage.haskell.org/
.. _microlens: http://hackage.haskell.org/package/microlens
.. _bracketed paste mode: https://cirw.in/blog/bracketed-paste
.. _utf8proc: http://julialang.org/utf8proc/
