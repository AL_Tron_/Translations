# Beginner's Guide to Installing from Source

First published on: August 28, 2015
Categories: Linux

# Введение(Introduction)

Этот(This) документ есмь предназначен(intended) для пользователей операционных систем с открытым исходным кодом(source), которые желают(wish) установить программное обеспечение непосредственно(direct) от оригинальных авторов, а не(rather than) полагаться(relying) исключительно(solely) на программное обеспечение (и версий), предоставляемые(provided) их(their) операционной системой. Это(It) есмь написан(written) для тех, кто не знаком(familiar) с концепцией загрузки(downloading) программного обеспечения в форме исходного(source) кода, и содержит(providing) справочную(background) информацию о соответствующих(relevant) командах и процессе в целом(general).

Обсуждаемые концепции(Concepts Discussed)

- Разработка(Development) и распространение(distribution): независимые(independent) разработчики(developers) программного обеспечения, поддержка нескольких ОС(multi-os), выпуски(releases) (архивы(archives)), контроль версий
- Дистрибутивы: бинарные пакеты и менеджеры пакетов
- Загрузка: http, ftp, wget, контрольные суммы и подписи
- Архивные файлы: tar, zip, gzip
- Общие файлы: README и INSTALL; необходимые зависимости
- Исправление с помощью sed/awk/patch
- Сборка(Building) и Инсталяция: настройка, создание, cmake, perl и python
- Документация по сборке(Building) и инсталяции 
- Настройки компилятора(Compiler settings) и удаление(stripping); передача ошибок
- Пост-инсталяция

# Разработка и распространение(Development and Distribution)

Типичная операционная система состоит из сотен различных приложений. В проприетарной операционной системе производитель(producer)/продавец(seller) этой операционной системы типично владеет(owns) и управляет(manages) разработкой(development) всего этого программного обеспечения. Однако системы с открытым исходным кодом обычно(usually) создаются(created) путем получения(obtaining) и интеграции(integrating) приложений, изобретенных(invented) и поддерживаемых(maintained) многими независимыми(independent) группами - или даже отдельными(single) независимыми разработчиками. Вдобавок, проект программного обеспечения с открытым исходным кодом часто используется в различных операционных системах-например, на базе Linux, BSD, Hurd, а иногда даже интегрируется в собственные операционные системы, такие как Solaris, Mac, даже Windows (если позволяет лицензия).

Разработчики оригинального программного обеспечения иногда упоминаются(referred) сопровождающими(maintainers) дистрибутива(distribution) и конечными пользователями как вышестоящий(upstream) источник(source) или просто вышестоящий(upstream). Точно так же(Similarly) разработчики часто ссылаются(refer) на дистрибутивы(distributions), которые используют их код, или на конечных пользователей как на нижестоящие(downstream).

Большинство(Most) проектов с открытым исходным кодом(source) хранят(store) свой(their) исходный код в системе контроля версий, доступной(accessible) через Интернет (только для чтения анонимными пользователями). Для таких проектов можно(it is possible) загрузить самые последние(very latest) “незавершенные(in-progress)” файлы или загрузить набор(set) файлов, “помеченных(tagged)” некоторым идентификатором выпуска(release-id) (номером версии). Однако это(it) есмь часто(often) не очень эффективно делать; проекты обычно выпускают(make) регулярные выпуски(releases), создавая(creating) архивный файл, содержащий(containing) все соответствующие(relevant) файлы, и делая(make) это доступным(available) для загрузки. Для проектов, не имеющих общедоступной(public) системы контроля версий, такие периодически выпускаемые(released) архивные файлы являются(are) единственным способом(the only way) прямого(directly) получения(obtain) исходного кода(sourcecode).

# Дистрибутивы(Distributions), Бинарные(Binary) Пакеты и Компиляция(Compiling) из исходного Кода(Source)

Существует(There are) множество дистрибутивов операционных систем, которые делают работу по поиску(finding), загрузке(downloading) и настройке(tweaking) всех наиболее(most) часто используемых(used) пакетов программного обеспечения. Некоторые(Several) также(also) компилируют(compile) это(it) и делают(make) доступными(available) результаты в бинарной форме. Есть (There are) много преимуществ(benefits), включая более быструю(quicker) инсталляцию, одно(one) место(location) для поиска(find) соответствующего(relevant) программного обеспечения и, в частности(particularly), поставка(supply) соответствующих(relevant) исправлений безопасности(security patches): сопровождающие дистрибутива(distribution maintainers) смотрят(watch) за обновлениями безопасности(security updates) и облегчают их установку конечным пользователям(make it easy for end users to notice and install them).

Однако такие дистрибутивы часто не включают очень самую последнюю версию программного обеспечения; если вам нужна(need) более новая(newer) версия, чем та, которая предоставляется(the one provided) вашим дистрибутивом, то, возможно(it may be), потребуется(necessary) собрать(build) её(it) самостоятельно(yourself). Программное обеспечение есмь также часто очень легко настраивается(customisable) во время установки(install-time), особенно(particularly) с возможностью(ability) опустить(omit) возможности, которые не есмь нужные(features that are not needed). Поскольку(Because) дистрибутив должен радовать широкий круг пользователей(needs to keep a wide range of users happy), они, как правило(tend), включают каждую возможность(feature) в скомпилированные приложения(applications), которые они распространяют(distribute); вы, как конечный пользователь(end-user), может быть в состоянии(able) настроить(to tune) приложение(application) лучше, компилируя это(it) самостоятельно(yourself).

Дистрибутивы генерально включают пакет менеджер, который хранит(keeps) базу данных(database), в которой программное обеспечение есмь инсталлировано. Это есмь не хорошая идея, чтобы пойти менять(to go changing) программное обеспечение на машине “за спиной(behind the back)” менеджера пакетов; позже может произойти всякое странное поведение(all sorts of odd behaviour could occur later). Если в вашем дистрибутиве есть пакет менеджер, пожалуйста, прочтите этот документ для получения справочной(background) информации, но затем обратитесь(consult) к документации для вашего дистрибутива, чтобы узнать(find out), как установить(install) программное обеспечение, не сбивая с толку(without confusing your) менеджера пакетов. Обычно(Usually) это включает(involves) в себя написание спецификации пакета, описывающей(describes) программное обеспечение, а затем использование локального менеджера пакетов для компиляции и установки(install) программного обеспечения. Это(This) относится(applies) ко всем дистрибутивам с пакет-менеджерами, независимо(regardless) от того(of whether), распространяются ли приложения в бинарном (предварительно скомпилированном(pre-compiled)) или исходном коде(source-code).

# Загружание и безопасность(Downloading and Security)

Обычный(usual) путь(way) загрузить(download) архивный файл есмь используя(by using) web-браузер and щёлкая(clicking) на “загрузить(download)” кнопку, щёлкая ссылку, или право-щелкание(right-clicking) ссылки и выбирание “сохранить как(save as)”. Это(This) загрузка файла используя “http” протокол. Некоторые(Some) сайты вместо(instead) этого делают(make) файлы доступными(available) через более старый “ftp” протокол. Много web-браузеров поддерживают это тоже, т. е. щелкание ссылки может также работать здесь. Альтернативно, там есмь ftp клиентские приложения.

“wget” или “curl” command-line приложения могут также быть использованы под unix-подобными операционными системами, чтобы скачать(fetch) файл через http или ftp когда корректный URL есмь известен.

Сайты, которые публикуют программное обеспечение, которое есмь очень часто(frequently) загружается, часто имеют “зеркальные(mirror)” сайты (set up), т. е. полезные(helpful) люди хранят(keep) копии файлов в разных местах по всему миру(downloaded often have “mirror” sites set up, ie helpful people keep copies of the files in various places around the world).
Такой(The) оригинальный сайт обычно имеет список доступных зеркал и вы должны выбрать один, который есмь ближе к вам.
Это помогает снизить(reduce) стоимость пропускной способности сети(cost of network bandwidth) на оригинальном издателе(publisher), а также, как правило, позволяет(usually also lets you) загружать быстрее.

Оба “http” и “ftp” сетевые протоколы могут быть перехвачены(intercepted) криминалами или другими нежелательными сторонами(parties), кто может затем модицировать данные по мере их загрузки(as it downloads). Данные могут также потенциально быть повреждены(corrupted) во время работы(accident while underway) (хотя это нечасто(is not common)). И когда, используя зеркало-сайт(And when using a mirror-site), это есмь возможно, что кто-то(someone) изменил(modified) файлы на нем (т. е. файлы на зеркале не совпадают(same) с файлами исходного издателя(original publisher)). Поэтому рекомендуется убедиться, что то, что вы скачали, соответствует задумке исходного издателя(It is therefore a good idea to verify that what you downloaded is what the original publisher intended).

Многие сайты предоставляют(provide) файл md5sum для каждого файла архива, который держит(holds) контрольную сумму(checksum) содержимого файла; иногда один файл md5sums содержит контрольные суммы для ряда других файлов(sometimes a single md5sums file holds checksums for a number of other files). Вы должны всегда(always) получать(obtain) файл md5sums с исходного(original) сайта, а не с зеркала(never a mirror). И вы должны загрузить это через защищенный(secure) протокол https, если  возможно. Затем инструмент командной строки unix md5sum можно использовать для вычисления контрольной суммы большого архивного файла и сравнения ее с ожидаемыми значениями, чтобы убедиться, что содержимое соответствует ожиданиям(The unix md5sum commandline tool can then be used to compute the checksum of the big archive file, and compare it to the expected values to ensure the contents are as expected).

Для вычисления(compute) sum одиночного(single) файла:

    md5sum file-to-check

и “вручную(by hand)” проверьте(verify) вывод(output) этого(this) приложения(application) на соответствие(against) ожидаемому(expected) значению(value). Если значение есмь на web-странице, вы можете открыть диалог “найти(find)” на этой странице и скопировать-и-вставить(copy-and-paste) значение, выведенное программой md5sum. Тот “найти(find)” будет совпадать(match) только  если значения есмь то самое.

Если поставщик(provider) программного обеспечения предоставляет(provides) файл md5sums, который имеет список пар (имя файла, контрольная сумма(checksum)), (then) вы можете запустить(run):

    md5sum -c md5sums-file

который будет искать в вашей локальной системе каждый файл, указанный в файле md5sums, вычислять его контрольную сумму и сообщать об ошибке, если она не соответствует ожидаемому значению.

Некоторые поставщики программного обеспечения подписывают(sign) архивные файлы вместо (или также) предоставления контрольной суммы md5. В этом случае вам следует:

- загрузите открытый ключ поставщика с их web-сайта (используя https, где возможно(possible))
- загрузите “файл подписи” для архивного файла; это будет небольшой файл с тем же базовым именем, что и загруженный файл, с суффиксом “.sig” или “.asc”
- выполните(perform) следующие действия



    # needed only once for each key, ie each "publisher"
    gpg --import {public-key}

    gpg --verify {signature-file-name}


Шаг проверки(verify) расшифровывает(decrypts) файл подписи(signature-file), показывая(revealing) контрольную сумму(checksum); затем он запускает алгоритм контрольной суммы над реальным файлом и проверяет, совпадают ли они(it then runs a checksum algorithm over the real file and checks that they are the same). Очевидно(Obviously), что приложение “gpg” должно быть установлено(needs to be installed) локально.

# Архивные файлы

Архив есмь один(single) файл, содержащий(contains) в себе ряд(within it a number) других файлов. Существует(There are) несколько(several) различных(different) инструментов(tools) для создания таких файлов, но во всех случаях(cases) процесс(process) есмь эффективно(effectively) объединить(to append) все индивидуальные файлы вместе, а затем добавить “индекс”, содержащий смещение, длину, имя и другие свойства исходных файлов, чтобы их можно было извлечь снова
(which contains the offset, length, name and other properties of the original files so that they can be extracted again).

Замечание: слово “архив” в Английском может означать(imply) что-то “старое”, больше не используемое, или “резервную” копию. Хотя архивные файлы можно использовать для хранения резервных копий или редко используемых данных, они также удобны для передачи данных по сети.

Приложение “tar” пакует несколько(multiple) файлов в один(single) архивный файл “tar-формата”, который есмь наиболее часто используемым форматом с открытым исходным кодом. Tar оригинально означал(meant) “tape archive”, но этот формат работает хорошо на дисках тоже. Архивы Tar запоминают не только исходные имена файлов, но и их оригинальный Unix “идентификатор владельца(owner id)” и права доступа к файлам(file-permissions). Идентификатор владельца(ownerid) есмь редко(seldom) полезен (если только архив tar не был создан на той же машине, на которой он распаковывается(unless the tar archive was created on the same machine it is being unpacked on)), но права доступа к файлам есть(file-permissions are). По соглашению(By convention), файлам tar-формата обычно присваиваются имена, заканчивающиеся(are usually given names ending) на “.tar”. Файлы tar-формата, содержащие исходный код, иногда называют “tarballs"(containing sourcecode are sometimes referred to as).

Приложение tar не сжимает содержимое файла. Однако файлы исходного кода сжимаются очень хорошо, а пропускная способность сети всегда слишком медленная и слишком дорогая, поэтому файлы tar обычно сжимаются после создания с использованием универсального приложения для сжатия(source-code files do compress very well and network bandwidth is always too slow and too expensive, so tar files are usually compressed after creation, using a generic compression application). Наиболее часто используемый тип сжатия(most commonly used compression type is) - “gzip”, и результирующему файлу обычно присваивается суффикс “.tar.gz” или “.tgz”. Сжатие с помощью bzip2 также распространено, и таким файлам обычно присваивается суффикс “.tar.bz2”. Иногда будет использоваться сжатие “xz”; файлам обычно присваивается(Compression with bzip2 is also common, and such files are usually given the suffix “.tar.bz2”. Occasionally “xz” compression will be used; files are usually given) суффикс “.tar.xz”

Оригинальная версия приложения tar была создана давным-давно, и она несколько раз переосмысливалась. К сожалению, это означает, что доступные параметры командной строки значительно различаются в зависимости от того, какую версию приложения вы используете. Функциональность также варьируется в зависимости от версии; в частности, некоторые версии могут автоматически определять, когда было использовано сжатие, и автоматически распаковываться, в то время как другие требуют передачи правильных параметров командной строки для обработки сжатых файлов, а другие требуют, чтобы файл был распакован первым. Вот несколько примеров команд для извлечения файлов из архива tar:
The original version of the tar application was created a long time ago, and it has been reimplemented a number of times. Sadly this means that the commandline options available vary considerably depending on which version of the application you are using. The functionality also varies depending on version; in particular some versions can auto-detect when compression has been used, and decompress automatically while others require the correct commandline parameters to be passed in order to handle compressed files, and yet others require the file to be decompressed first. Here are some example commands to extract files from a tar archive:

    # Modern GNU tar options. This works for files compressed
    # with gzip and bzip too
    tar --extract --file filename

    # Same as above
    tar -xf filename

    # Same as above. Leading "-" is optional
    tar xf filename

    # explicitly decompress gzip2-compressed file then
    # pass uncompressed result directly into tar
    gzip -cd filename.tgz | tar xf -

    # same as above, but for bzip2-compressed files
    bunzip2 -cd filename.tar.bz2 | tar xf -

Обратите внимание, что если “f” опущено, tar просто зависает (ожидает ввода пользователя).
Note that if the “f” is omitted, tar just appears to hang (waits for user input).

Предупреждение(Warning): распаковка(unpacking) файла tar может перезаписать(overwrite) локальные файлы. По умолчанию файлы распаковываются(unpacked) в поддиректорию текущей директории, который должен быть безопасным(safe), если ваш текущий каталог является подходящим(as long as your current directory is something appropriate). Однако не доверяйте никаким(any) инструкциям, в которых используются следующие параметры; они могут пытаться обмануть вас, чтобы изменить(that use the following options; they may be trying to trick you into modifying) важные локальные файлы:

    -C or --directory
    -P or --absolute-names
    --transform or --xform

Большинство tar-файлов создаются таким образом, что при их распаковке создается один подкаталог в текущем каталоге, а все остальные файлы помещаются в этот каталог; например, распаковка файла с именем “foo-1.2.tar.gz” создаст подкаталог с именем “foo-1.2” с файлами внутри этого каталога. К сожалению, не все, кто упаковывает исходный код для загрузки, следуют этому соглашению; иногда файл tarfile расширяет свое содержимое непосредственно в текущий каталог, что может создать большой беспорядок, если в этом каталоге уже есть файлы. Поэтому лучше всего проверить содержимое файла tarfile перед распаковкой, используя следующую команду:

    # Modern GNU tar
    tar --list --file filename

    # Same as above
    tar -tf filename

    # Same as above. Leading "-" is optional
    tar tf filename

    gzip -cd filename | tar tf -
    bunzip -cd filename | tar tf -

Иногда(Occasionally) архивные файлы распространяются(distributed) в “zip формате”. Zip-архивы наиболее распространены в мире DOS и Windows или в связи с Java, но иногда встречаются в других местах. Формат Zip работает как комбинация tar и gzip (он использует то же сжатие, что и gzip, но имеет собственную внутреннюю “индексацию”). Файлы Zipfiles не сохраняют исходный идентификатор владельца unix или права доступа к файлам. Содержимое такого файла можно извлечь с помощью команды распаковать (при условии, что он установлен локально).

Где возможно, архивные файлы следует распаковывать при входе в систему(should be unpacked when logged in) как нормальный пользователь, не тот “root” пользователь. Это дополнительная мера безопасности для предотвращения любой непреднамеренной перезаписи файлов. Однако настройки владельца файла, записанные в tar-файле, сохраняются только в том случае, если пользователь, распаковывающий архив, является пользователем root. Если они важны (что не часто встречается), то архив должен быть распакован “как root”.
(This is an extra safety-measure to prevent any unintended file overwrites. However the file-owner settings recorded in the tar-file are only preserved when the user unpacking the archive is the root user. If these are important (which is not common) then the archive must be unpacked “as root”).

# Общие(Common) файлы

Unpacked source-code archives usually contain a file named README or INSTALL (or both) in the top-level directory. You should always read these documents first, as they give important information about how to compile, install and configure the rest of the source-code in the downloaded archive.

One important piece of information usually found in the README or INSTALL is a list of other software that must be installed first. Any program you download will require some local header-files, library-files and/or helper tools to build or to run. If you don’t get the prerequisites right, then the application may not compile, or may compile but not run, or may get built without certain optional features that you want.

Another important piece of information is the set of parameters that can be passed to the build-process; see later.


# Исправление(Patching)

Иногда загруженное программное обеспечение есмь известно не работает в вашей среде, и кто-то уже придумал, как настроить его для решения проблемы. Персона, кто решил ту проблему, может опубликовать свою “настройку(tweak)” в виде команды sed или awk, сценария оболочки, содержащего несколько команд sed/awk, или файла исправлений(patch). Очевидно, что вам нужно быть осторожным с такими изменениями, применяя их только в том случае, если вы доверяете источнику или имеете возможность перепроверить изменения.

Инструмент sed применяет регулярное выражение к каждой строке текстового файла и заменяет соответствующий текст чем-то другим. Это довольно ограниченный инструмент, но простой в использовании и широко доступный.

awk делает нечто подобное, но способен выполнять более сложные преобразования текстовых файлов.

Файл исправлений(patchfile) создается с помощью(is created by using) инструмента “diff” для сравнения двух версий одного и того же файла и вывода различий(to compare two versions of the same file and output the differences). Инструмент исправления(patch) может взять(take) вывод(output) “diff” и применить его к одному из файлов(apply it to one of the files), чтобы “преобразовать его(convert it)” в другую версию. Хорошая вещь в файлах исправлений (т. е. выводе diff) заключается в том, что они вполне удобочитаемы для человека, т. е. Достаточно легко увидеть, какие изменения будут внесены(The nice thing about patchfiles (ie the output of diff) is that it is quite human-readable, ie it is reasonably easy to see what changes will be made).

# Build Systems: configure, make, cmake, etc

Разработчики программного обеспечения кто создал программное обеспечение, которое вы только что загрузили, очевидно, нуждаются в каком-то способе компиляции и установки этого программного обеспечения на своих собственных компьютерах(you just downloaded obviously need some way of compiling and installing that software on their own machines). Любые конфигурационные файлы, необходимые для используемых ими инструментов(Whatever config files are necessary for the tools they use are), почти всегда включены в архивный файл. Поскольку разработчики с открытым исходным кодом хотят, чтобы как можно больше людей использовали их программное обеспечение, они также прилагают некоторые усилия, чтобы упростить сборку и установку программного обеспечения в различных системах(As open-source developers want as many people as possible to use their software, they also go to some effort to make it easy to build and install the software on a range). Однако они не могут поддерживать все(every) возможные конфигурации в мире.

В конце концов(In the end), смысл процесса установки состоит в том, чтобы преобразовать исходный исходный код в форму, которую может выполнить локальный компьютер, а затем поместить все необходимые файлы в каталоги, перечисленные(the point of the installation process is to convert the original source-code into a form that the local computer can execute, and then to place all the necessary files into directories that are listed) в переменной $PATH для всех пользователей (чтобы они могли выполнять эти файлы(so they can execute those files)). Инсталяция модулей для интерпретируемых языков есмь немного(slightly) отличная(different); эти(those) файлы устанавливаются(get installed), где интерпретатор (например, python или perl) может их найти, а не в $PATH напрямую(rather than in $PATH directly).

## Configure and Make

Наиболее распространенным инструментом, используемым для управления компиляцией и установкой исходного кода, является make(The most widely-spread tool used to manage the compilation and installation of source-code is make). Make есмь приложение, которое принимает файл конфигурации (называемый файлом создания), содержащий список правил, большую часть(takes a configuration-file (called a makefile) that contains a list of rules, most) формы:

- когда TARGET-FILE is older than SOURCE-FILE then SOME-ACTION

Нефортунально, хотя(although) синтаксис makefile есмь очень мощный(powerful), его все еще недостаточно для обработки всех возможных способов настройки компьютеров и всех возможных способов, которыми человек, устанавливающий (it still isn’t enough to be able to handle all the possible ways that computers can be configured, and all the possible ways that the person installing) программное обеспечение, может пожелать скомпилировать приложение. Поэтому многие программные пакеты поставляются со сценарием оболочки с именем “настройка(configure)” и файлом создания шаблона с именем “Makefile.in”. Скрипт настройки(configure) берет список параметров конфигурации в командной строке и преобразует makefile шаблона в реальный makefile, настроенный для локального компьютера и пожеланий установщика. Поэтому последовательность установки обычно выглядит следующим образом:

    # unpack and read documentation
    tar xf filename
    cd {directory created by above step}
    less README
    less INSTALL

    # generate customised makefile
    ./configure {some options ...}

    # compile everything in the local directory
    make

    # update global directories
    sudo make install

Кстати(By the way): скрипт “configure” автоматически генерируется программным обеспечением под названием autotools, но это не имеет значения для человека, который его использует.

Конфигурирование обычно вызывается как “./configure”, чтобы избежать двух возможных проблем:

- Некоторые пользователи не имеют “.” в их переменной $PATH. В частности(particular), the root пользователь не имеет это по причинам безопасности
- Некоторые пользователи не имеют “.” в качестве первой записи в их $PATH, что означает, что может быть запущен неправильный сценарий настройки(as the first entry in their $PATH, meaning that the wrong configuration script might get run)

В общем(general), лучше всего выполнять(to perform) все шаги, кроме “make install”, как нормальный системный пользователь, а не root. Это позволяет избежать ошибок и, возможно, некоторых атак (хотя, поскольку шаг установки выполняется от имени root, это не обеспечивает достаточной защиты)(This avoids mistakes and possibly some attacks (though as the install step is done as root that isn’t much protection)).

Некоторые проекты предоставляют(provide) файл makefile, но не скрипт “configure”; в этом случае шаг “configure” выше может быть опущен(omitted). Либо приложение достаточно простое, чтобы в нем не было необходимости(is simple enough that it doesn’t need it), либо разработчики программного обеспечения встроили больше логики в написанный от руки файл makefile(have built more logic into the hand-written makefile).

Не все системы настроены(are set up) с включенным(enabled) "sudo". В этом случае вместо этого используйте следующее:

    su  # must then enter root password
    make install
    exit

Для большинства программ команды configure и make могут выполняться(run) в той же директории, что и исходный код, как показано выше(as the source-code, as shown above). В результате новые файлы, созданные во время компиляции, смешиваются с оригиналами, что несколько неаккуратно, но команду “make clean” можно использовать для очистки позже(result is that new files generated during compilation get mixed together with the originals, which is somewhat messy, but the “make clean” command can be used to tidy that up later). Однако для некоторых программ необходимо создать временный каталог, изменить текущий рабочий каталог на этот каталог, а затем выполнить шаги сборки там; в проектной документации должно быть указано, необходимо ли это(it is necessary to create a temporary directory, change the current-working-directory to that directory and then perform the build-steps there; the project documentation should indicate if this is necessary). Некоторые люди считают, что лучше всегда создавать из временной директории(consider it better to always build from a temporary directory). Пример построения с использованием отдельного каталога рядом с исходным исходным кодом, что является общим соглашением(building using a separate directory next to the original source-code, which is a common convention):

    # unpack into a directory {packagename}
    tar xf filename

    # create separate build directory
    mkdir {packagename}-build

    # compile everything in the separate build directory
    cd {packagename}-build
    ../{packagename}/configure {some options}
    make

    # update global directories
    sudo make install

## Other Build Tools

Некоторые проекты используют cmake в качестве инструмента для сборки(as their build-tool). cmake работает примерно так же, как configure(somewhat like configure) (см. выше); это генерирует файл makefile, содержимое которого зависит от параметров, переданных команде cmake, и от особенностей(whose content depends on the options passed to the cmake command, and on features) локальной системы. Шаги, необходимые для создания пакета на основе cmake, идентичны приведенным выше примерам “configure/make”, за исключением того, что шаг настройки заменен(steps require to build a cmake-based package are identical to the “configure/make” examples above except that the configure step is replaced by):

    cmake . -DCMAKE_BUILD_TYPE=Release {some options ...}

Как всегда, ознакомьтесь с документацией проекта для получения инструкций о том, как строить(check the project’s documentation for instructions on how to build).

Некоторые проекты используют инструменты сборки на основе python или perl, а не make(build-tools based on python or perl rather than make). Принципы все еще довольно схожи(are still fairly similar).

Программное обеспечение, которое не нуждается в компиляции, обычно имеет довольно простой и быстрый процесс установки(does not need to be compiled usually has a fairly simple and quick installation process). В частности(particular), приложения, написанные на языках с интерпретацией Perl или Python, можно установить, просто скопировав файлы в соответствующее место(written in the Perl or Python interpreted languages can be installed just by copying the files into the relevant location). Эти(These) проекты, тем не менее, включают в себя программу или скрипт в файле архива, который выполняет эту задачу, а не требует, чтобы установщик делал это вручную
(nevertheless include a program or script in the archive-file which performs this task, rather that requiring the installer to do this manually).

## Environment Variables

Опции конфигурации компиляции и инсталляции приложения обычно передаются в качестве параметров командной строки(passed as command-line parameters) в скрипт “configure” или в программу make. Однако иногда опции конфигурации передаются вместо этого через переменные среды(are passed via environment variables instead). Они(These) могут быть указаны(specified) путем размещения определений в начале команды(by placing the definitions on the start of the command), например

    NAME=tom ENABLE_FOO=no ./configure

Environment variables can also be defined before running the command:

    export NAME=tom
    export ENABLE_FOO=no
    ./configure

Какие(Which) опции доступны, нормально описано в файле README архива или УСТАНОВОЧНЫХ файлах, или на web-сайте проекта. Иногда доступные опции можно просмотреть, запустив(seen by running) ./configure --help.

## Building и Installing Документация

Некоторые проекты предоставляют документацию, которая может быть “инсталирована(installed)”, чтобы она была доступна через обычные средства просмотра документации системы, такие как “человек(man)” или “информация(info)”. Некоторые предоставляют документацию в формате HTML, которая обычно устанавливается в /usr/share/doc. Иногда эта документация включается в “стандартный(standard)” архивный файл, а иногда это отдельная (необязательная) загрузка. Иногда документация устанавливается как часть стандартной команды make install, а иногда для ее установки необходимо использовать отдельную команду. И иногда документация поставляется в “готовой к использованию(ready-to-use)” форме, но иногда она поставляется в своего рода “сырой форме(raw form)”, которая должна быть обработана перед установкой-скорее, как исходный код должен быть скомпилирован.

Как уже должно быть ясно, разнообразие подходов к предоставлению документации настолько велико, что здесь нельзя дать никаких действительно полезных советов(As should be clear by now, the variety of approaches to delivering documentation is so wide that no really useful advice can be given here). См. файлы README и INSTALL в загруженном архиве, а также web-сайт проекта для получения инструкций(guidance).

## Другие Цели Сборки(Other Build Targets)

Помимо(As well as) команд для компиляции всего(everything) (“make”) и инсталяции ранее скомпилированных(previously-compiled) программ (“make install”) или документации, существует несколько(few) других распространенных(common) возможностей(possibilities).

make clean обычно удаляет все сгенерированные файлы, т. е. оставляет директорию такой, какой она была после распаковки файлов из архива(leaves the directory as it was after the files were unpacked from the archive).

## Invoking a Compiler

Как отмечалось выше, наиболее общим(common) шагом, выполняемым “make” или “cmake”, является вызов(to invoke) компилятора. В локальной системе должны быть установлены соответствующие(appropriate) компиляторы.

Это также шаг, который, скорее всего, завершится неудачей(This is also the step that is most likely to fail) (вместе со связыванием(along with linking)).

Если на шаге компиляции происходит сбой(fails) с сообщением об ошибке(error-message) о том, что не удалось найти файл заголовка(not being able to find a header-file) или файл библиотеки(not being able to find a library-file), то, вероятно, вы не установили все предварительные условия(then you have probably not installed all of the pre-requisites)-перечитайте файлы README и INSTALL. В некоторых случаях отсутствующее(missing) предварительное условие(prerequisite) является опциональным, и в этом случае будет параметр, который можно передать для настройки, или переменная среды, которая может быть настроена таким образом, чтобы программное обеспечение можно было устанавливать без этого предварительного условия(in which case there will be a parameter that can be passed to configure or an environment variable which can be set to allow the software to be installed without that prerequisite). Дважды проверьте(Double-check) указанные вами(you specified) параметры, и если они кажутся корректными, то проектная документация является лучшим ресурсом для решения таких проблем(issues).

Компиляторы имеют ряд опций, которые потенциально могут повысить производительность. Однако вам следует возиться с ними только в том случае, если у вас достаточно опыта. Если вам нужен этот документ, просто оставьте параметры компилятора по умолчанию!
(Compilers have a range of options that can potentially improve performance. However you should only mess with these if you have plenty of experience. If you need this document, then just leave compiler options at their defaults!)

Результаты(The output) компиляции и компоновки (“executables”, которые вы действительно хотели) обычно содержат значительные объемы данных, которые полезны для отладки программ, но не полезны для “нормальных конечных пользователей(normal end users)". Эту информацию можно удалить из исполняемых файлов, запустив на них strip {имя файла}. Небольшие программы сэкономят место на диске, а также будут загружаться немного быстрее. Если вы не собираетесь отлаживать(debug) программу, использование strip есмь хорошая идея.

# Пост-Инсталяционная Конфигурация

Некоторые приложения могут настраивать свое поведение через файлы конфигурации, считываемые при запуске приложения(Some applications can have their behaviour customised via configuration files read on application startup). Часто приложения инсталлируют версии файлов конфигурации по умолчанию где-то в директории /usr или в директории /etc. Проверьте вывод команды make install, чтобы узнать, какие файлы конфигурации были установлены(Inspect the output of the make install command to see which config files have been installed). Опции конфигурации также должны быть задокументированы в README или INSTALL программы или на их web-сайте(Configuration options should also be documented in the program’s README or INSTALL, or on their website).

# Appendix A: An example Makefile

Нефортунально, при сборке пакетов из исходного кода нередко возникают ошибки компиляции. Иногда можно диагностировать и устранить проблему, проверив файл makefile, т. е. базовое понимание синтаксиса файла makefile может быть полезным. Здесь приведен очень краткий пример базового синтаксиса и функциональности; для получения дополнительной информации см. документацию make или одно из многочисленных учебных пособий, доступных на линии(online).

Пример makefile для сборки исполняемого файла под названием ‘prog’, который имеет один исходный файл(sourcefile) ‘c’, один файл заголовка(headerfile) и использует одну библиотеку (которую он также собирает из одного исходного файла(sourcefile) ‘c’), может выглядеть следующим образом(could look like the following):

    prog: prog.c prog.h libmylib.a
    gcc -o prog prog.c -L. -lmylib

    libmylib.a: libmylib.o
    ar -rcs libmylib.a libmylib.o

    libmylib.o: libmylib.c libmylib.h
    gcc -c -o libmylib.o libmylib.c

Форма записей(entries) (правил(rules)) есмь:

    target: dependency1 [dependency-n ...]
    <tab> command to execute
    ...

Для каждого “правила”, если цель(target) отсутствует(missing) или старше(older) любой из зависимостей (на основе временных меток файла(based on file timestamps)), выполняются команды(then the command(s) are run) для (пере-)создания цели. Однако каждая зависимость сначала(first) проверяется(tested), чтобы определить(to see), существует ли правило, для которого она является целевой(if there is a rule that has it as a target). Если так, то эта цель рекурсивно оценивается(evaluated), т. е. (пере-)собирается(built), если она отсутствует или старше, чем ее зависимости.

Таким образом, в приведенном выше наборе правил изменение в libmylib.c приведет к перестройке libmylib.o. Затем libmylib.a восстанавливается и, наконец, prog перестраивается.
(Thus in the above set of rules, a change in libmylib.c will cause libmylib.o to be rebuilt. Then libmylib.a is regenerated and finally prog is rebuilt.)

Make-файлы(Makefiles) могут быть очень сложными, и многие из них генерируются автоматически, но всегда применяются вышеуказанные принципы.
(Makefiles can get very complicated and many are automatically generated, but the above principles always apply)

# Благодарности

Этот документ был вдохновлен программным обеспечением для создания документов TLDP, которое, к сожалению, активно не поддерживается.
(inspired by the TLDP document Software Building HOWTO which sadly is not actively maintained).
