3. Использование Make

Makefile - ключ к процессу сборки. В его простейшей форме, Makefile - скрипт для компиляции(compiling) и построения(building) "бинарников(binaries)", исполняемых порций пакета. Makefile может также предоставить средства обновления пакета программного обеспечения без необходимости(having) перекомпилировать каждый файл источника в нем(it), но это другая история (или другая статья).

В какой-то момент Makefile запускает cc или gcc. Это на самом деле(actually) препроцессор, компилятор C (или C++) и компоновщик(linker), вызываемый в этом порядке. Этот процесс преобразует исходник(source) в бинарники(binaries), фактические исполняемые файлы.

Для вызова(Invoking) make обычно достаточно просто набрать make. Обычно это создает все необходимые исполняемые файлы для рассматриваемого пакета. Однако, make может также выполнять(do) другие задачи, такие как установка файлов в соответствующих каталогах (make install) и удаление устаревших(stale) объектных файлов(make clean).  Выполнение(Running) make -n позволяет(permits) предварительно просмотреть(previewing) процесс сборки(build), поскольку он распечатывает все команды, которые будут запущены make, не выполняем их.

Только простейшее программное обеспечение использует общий(generic) Makefile. Более сложные(complex) установки требуют настройки Makefile в соответствии с расположением(location) библиотек, включаемых(include) файлов и ресурсов на вашем конкретном(particular) компьютере(machine). Это особенно актуально , когда для сборки требуются библиотеки X11. Imake и xmkmf справляются с этой задачей.

Imakefile, если(is, to) цитировать(quote) страницу рукводства(man), "шаблон(template)" Makefile. imake утилита конструирует Makefile подходящий(appropriate) для вашей системы из Imakefile. В почти всех случаях, однако, вы должны(would) run(запустить) xmkmf, shell скрипт, который вызывает imake, интерфейс(front end) для этого. Проверьте README или INSTALL файл, включенный(included) в архив программного обеспечения для специфических инструкций. (Если, после разархивации(dearchiving) исходных файлов, там есмь файл Imake представленный в базовой директории, это значит(dead giveaway), что xmkmf должен быть запущен.) Прочтите страницы руководства(man) Imake и xmkmf для более подробного анализа процедуры.

Имейте в виду, что xmkmf и make может потребоваться вызвать от имени пользователя root, особенно при выполнении make install для перемещения двоичных файлов в каталоги /usr/bin или /usr/local/bin. Использование make как ординарного пользователя без root привелегий, скорее всего, приведет к сообщениям об ошибках с отказом в доступе на запись(write access denied), поскольку у вас нет прав на запись в системные директории. Также убедитесь(Check), что созданные двоичные файлы(binaries) имеют надлежащие разрешения на выполнение для вас и любых других соответствующих пользователей.

При вызове(Invoking), xmkmf использует файл Imake, чтобы собрать(build) новый Makefile, подходящий(appropriate) для вашей системы. Обычно вы вызываете xmkmf с аргументом -a, чтобы автоматически делать make Makefiles, make includes, и make depend. Это устанавливает переменные и определяет(defines) расположение(locations) библиотек для компилятор и компоновщика. Иногда, там не будет файла Imake, вместо этого будет скрипт INSTALL или configure, который выполнит эту задачу(accomplish this purpose). Заметьте, что если вы запустите(run) configure, это должно быть вызвано(invoked) как ./configure, чтобы гарантировать, что вызывается правильный конфигурационный скрипт в текущей директории. В большинстве(most) случаев(cases), файл README включенный в дистрибутив(distribution), объясняет процедуру установки.

Обычно рекомендуется(good idea) визуально проверить Makefile, который собирается(builds) xmkmf или одним из скриптов установки.
Makefile, обычно(normally), подходит(correct) для вашей системы, но иногда вам может потребоваться "настроить(tweak)" его или исправить(correct) ошибки вручную(manually). but you may occasionally be required to "tweak" it or correct errors manually.

Installing the freshly built binaries into the appropriate system directories is usually a matter of running make install as root. The usual directories for system-wide binaries on modern Linux distributions are /usr/bin, /usr/X11R6/bin, and /usr/local/bin. The preferred directory for new packages is /usr/local/bin, as this will keep separate binaries not part of the original Linux installation.

Packages originally targeted for commercial versions of UNIX may attempt to install in the /opt or other unfamiliar directory. This will, of course, result in an installation error if the intended installation directory does not exist. The simplest way to deal with this is to create, as root, an /opt directory, let the package install there, then add that directory to the PATH environmental variable. Alternatively, you may create symbolic links to the /usr/local/bin directory.

Your general installation procedure will therefore be:

Read the README file and other applicable docs.
Run xmkmf -a, or the INSTALL or configure script.
Check the Makefile.
If necessary, run make clean, make Makefiles, make includes, and make depend.
Run make.
Check file permissions.
If necessary, run make install.
Notes:

You would not normally build a package as root. Doing an su to root is only necessary for installing the compiled binaries into system directories.
After becoming familiar with make and its uses, you may wish to add additional optimization options passed to gcc in the standard Makefile included or created in the package you are installing. Some of these common options are -O2, -fomit-frame-pointer, -funroll-loops, and -mpentium (if you are running a Pentium cpu). Use caution and good sense when modifying a Makefile!
After the make creates the binaries, you may wish to strip them. The strip command removes the symbolic debugging information from the binaries, and reduces their size, often drastically. This also disables debugging, of course.
The Pack Distribution Project offers a different approach to creating archived software packages, based on a set of Python scripting tools for managing symbolic links to files installed in separate collection directories. These archives are ordinary tarballs, but they install in /coll and /pack directories. You may find it necessary to download the Pack-Collection from the above site should you ever run across one of these distributions.
