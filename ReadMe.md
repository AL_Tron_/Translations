# Переводы на русский язык

- github.com
  - chrisdone/wesite - статьи "Typeable and Data in Haskell"
  - jtdaugherty/brick - руководство библиотеки, написанной на языке программирования Haskell
  - haskell-servant/servant - руководство библиотеки, написанной на языке программирования Haskell
    Оригинал:
      - https://github.com/haskell-servant/servant/
      - http://haskell-servant.readthedocs.io/en/stable/
            
- linuxfromscratch.org/lfs - Linux From Scratch
  - head-com - dev-версия в стиле technical-community(com), на базе перевода 9й версии, который находится по адресу: https://github.com/avmaisak/LFS_Book
  - head - dev-версия в дословном, техническом и литературном стиле
  - releases/10.1 - выпуск версии 10.1 в дословном, техническом и литературном стиле. 
    - com - в стиле technical-community(com)
    Перевод версии 10.0 можно найти в  по адресу https://github.com/zlietapki/linux_from_scratch_10_rus
    
- moi.vonos.net/MineOnInformationBeg-literal.md - Beginner's Guide to Installing from Source
- tldp.org/HOWTO/Software-Building-HOWTO.html - Building and Installing Software Packages for Linux

Поддержать:
- Bitcoin bc1qcm3g42nwtypp4vj8jm3wkjleussv05dk47l4r0
- Яндекс кошелёк 410011604674676 (можно указать на какой конкретно перевод отправлены деньги)

Вопросы и предложения, можно отправлять на почту:
altron.dev@gmail.com

Виды переводов:
- literal - дословный, с использованием собственных словарей
- technical - технический, с использованием собственных словарей
- literary - литературный
- technical-community(com) - технический, с использованием словарей, с которыми соглашаются IT-сообщества.
