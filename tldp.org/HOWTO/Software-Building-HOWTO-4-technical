4. Предупакованные(Prepackaged) бинарники(Binaries)

4.1 Что не так с rpms?

Ручная(Manually) сборка(building) и установка(installing) пакетов из исходного кода, видимо, настолько сложная задача для некоторых пользователей Linux, что они перешли на популярные форматы пакетов rpm и deb или более новый формат пакетов Stampede slp. Хотя может быть и так, что установка rpm обычно проходит так же гладко и быстро, как и установка программ в какой-нибудь другой пресловутой операционной системе, следует подумать о недостатках самостоятельной установки предварительно упакованных двоичных файлов.
(packages from source is apparently so daunting a task for some Linux users that they have embraced the popular rpm and deb or the newer Stampede slp package formats. While it may be the case that an rpm install normally runs as smoothly and as fast as a software install in a certain other notorious operating system, some thought should certainly be given to the disadvantages of self-installing, prepackaged binaries.)


Во-первых, имейте в виду, что пакеты программного обеспечения обычно выпускаются сначала в виде "tarballs", а готовые двоичные файлы - через несколько дней, недель и даже месяцев.
First, be aware that software packages are normally released first as "tarballs", and that prepackaged binaries follow days, weeks, even months later. A current rpm package is typically at least a couple of minor version behind the latest "tarball". So, if you wish to keep up with all the 'bleeding edge' software, you might not wish to wait for an rpm or deb to appear. Some less popular packages may never be rpm'ed.

Второе, пакет "tarball" может быть(may well be) более полным(complete), иметь больше опций и лучше поддаваться настройке(customization) и доработке(tweaking). В бинарной версии rpm может отсутствовать(missing) некоторая функциональность полного выпуска(release). Source rpm's contain the full source code and are equivalent to the corresponding "tarballs", and they likewise need to be built and installed using either of the rpm --recompile packagename.rpm or rpm --rebuild packagename.rpm options.

В-третьих, некоторые предварительно упакованные двоичные файлы не будут правильно установлены, и даже если они будут установлены, они могут дать сбой и дамп ядра.
Third, some prepackaged binaries will not properly install, and even if they do install, they could crash and core-dump. They may depend on different library versions than are present in your system, or they may be improperly prepared or just plain broken. In any case, when installing an rpm or deb you necessarily trust the expertise of the persons who have packaged it.

Finally, it helps to have the source code on hand, to be able to tinker with and learn from it. It is much more straightforward to have the source in the archive you are building the binaries from, and not in a separate source rpm.

Установка пакета rpm не всегда проста.
Installing an rpm package is not necessarily a no-brainer. If there is a dependency conflict, an rpm install will fail. Likewise, should the rpm require a different version of libraries than the ones present on your system, the install may not work, even if you create symbolic links to the missing libraries from the ones in place. Despite their convenience, rpm installs often fail for the same reasons "tarball" ones do.

You must install rpm's and deb's as root, in order to have the necessary write permissions, and this opens a potentially serious security hole, as you may inadvertently clobber system binaries and libraries, or even install a Trojan horse that might wreak havoc upon your system. It is therefore important to obtain rpm and deb packages from a "trusted source". In any case, you should run a 'signature check' (against the MD5 checksum) on the package, rpm --checksig packagename.rpm, before installing. Likewise highly recommended is running rpm -K --nopgp packagename.rpm. The corresponding commands for deb packages are dpkg -I | --info packagename.deb and dpkg -e | --control packagename.deb.

    rpm --checksig gnucash-1.1.23-4.i386.rpm

    gnucash-1.1.23-4.i386.rpm: size md5 OK

    rpm -K --nopgp gnucash-1.1.23-4.i386.rpm

    gnucash-1.1.23-4.i386.rpm: size md5 OK

Для истинно параноиков (и, в данном случае, можно много сказать о паранойе), в каталоге Sunsite utils / package доступны утилиты unrpm и rpmunpack для распаковки и проверки отдельных компонентов пакетов.
For the truly paranoid (and, in this case there is much to be said for paranoia), there are the unrpm and rpmunpack utilities available from the Sunsite utils/package directory for unpacking and checking the individual components of the packages.

для проверки целостности установленных файлов .deb по контрольным суммам MD5.
Klee Diene has written an experimental dpkgcert package for verifying the integrity of installed .deb files against MD5 checksums. It is available from the Debian ftp archive. The current package name / version is dpkgcert_0.2-4.1_all.deb. The Jim Pick Software site maintains an experimental server database to provide dpkgcert certificates for the packages in a typical Debian installation.

В своей наиболее простой форме команды rpm -i packagename.rpm и dpkg --install packagename.deb автоматически распаковывают и устанавливают программное обеспечение. Однако будьте осторожны, поскольку использование этих команд вслепую может быть опасно для здоровья вашей системы!
In their most simple form, the commands rpm -i packagename.rpm and dpkg --install packagename.deb automatically unpack and install the software. Exercise caution, though, since using these commands blindly may be dangerous to your system's health!

Обратите внимание, что приведенные выше предупреждения также относятся, хотя и в меньшей степени, к утилите установки Slackware pkgtool. Любая «автоматическая» установка программного обеспечения требует осторожности.
Note that the above warnings also apply, though to a lesser extent, to Slackware's pkgtool installation utility. All "automatic" software installations require caution.

Программы martian и alien позволяют конвертировать между форматами пакетов rpm, deb, Stampede slp и tar.gz. Это делает эти пакеты доступными для всех дистрибутивов Linux.
The martian and alien programs allow conversion between the rpm, deb, Stampede slp, and tar.gz package formats. This makes these packages accessible to all Linux distributions.

Внимательно прочтите справочные страницы для команд rpm и dpkg и обратитесь к RPM HOWTO, Краткому руководству TFUG по диспетчеру пакетов Red Hat и Инструментам управления пакетами Debian для получения более подробной информации.
Carefully read the man pages for the rpm and dpkg commands, and refer to the RPM HOWTO, TFUG's Quick Guide to Red Hat's Package Manager, and The Debian Package Management Tools for more detailed information.

4.2 Problems with rpms: an example

Ян Хубицка написал очень хороший фрактальный пакет под названием xaos. На его домашней странице доступны пакеты .tar.gz и rpm. Для удобства попробуем версию rpm, а не tarball.
Jan Hubicka wrote a very nice fractal package called xaos. At his home page, both .tar.gz and rpm packages are available. For the sake of convenience, let us try the rpm version, rather than the "tarball".

Unfortunately, the rpm of xaos fails to install. Two separate rpm versions misbehave.

rpm -i --test XaoS-3.0-1.i386.rpm

    error: failed dependencies:
            libslang.so.0 is needed by XaoS-3.0-1
            libpng.so.0 is needed by XaoS-3.0-1
            libaa.so.1 is needed by XaoS-3.0-1

rpm -i --test xaos-3.0-8.i386.rpm

    error: failed dependencies:
            libaa.so.1 is needed by xaos-3.0-8

The strange thing is that libslang.so.0, libpng.so.0, and libaa.so.1 are all present in /usr/lib on the system tested. The rpms of xaos must have been built with slightly different versions of those libraries, even if the release numbers are identical.

As a test, let us try installing xaos-3.0-8.i386.rpm with the --nodeps option to force the install. A trial run of xaos crashes.

    xaos: error in loading shared libraries: xaos: undefined symbol: __fabsl

Let us stubbornly try to get to the bottom of this. Running ldd on the xaos binary to find its library dependencies shows all the necessary shared libraries present. Running nm on the /usr/lib/libaa.so.1 library to list its symbolic references shows that it is indeed missing __fabsl. Of course, the absent reference could be missing from one of the other libraries... There is nothing to be done about that, short of replacing one or more libraries.

Enough! Download the "tarball", XaoS-3.0.tar.gz, available from the ftp site, as well as from the home page. Try building it. Running ./configure, make, and finally (as root) make install, works flawlessly.

This is one of an number of examples of prepackaged binaries being more trouble than they are worth. 
