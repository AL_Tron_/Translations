servant – A Type-Level Web DSL
==============================

.. image:: https://raw.githubusercontent.com/haskell-servant/servant/master/servant.png

**servant** есмь набор(set) Haskell-ных библиотек для написания *типо-безопасных* web приложений, а также *создания(deriving)* клиентов (в Haskell и других языках) или генерирования документации для них, и многое другое(more).

Это достигается(is achieved) путём(by) взятия(taking) в качестве(as) входных данных(input) описания web API как типа Haskell. Затем Servant может проверить(is then able to check), действительно ли ваши серверные обработчики запросов правильно реализуют ваш web API(that your server-side request handlers indeed implement your web API faithfully), или автоматически получить(derive) функции Haskell, которые могут воздействовать(hit) на web приложение, реализующее этот API, сгенерировать описание Swagger или код для клиентских функций в некоторых других языках прямо(directly).

Если вы хотите(would like) узнать(to learn) больше(more), нажмите(click) учебную(tutorial) ссылку ниже.

.. toctree::
  :maxdepth: 2

  tutorial/index.rst
  cookbook/index.rst
  examples.md
  links.rst
  principles.rst
